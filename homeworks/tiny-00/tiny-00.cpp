#include "tiny-00.hpp"

#include <ostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iomanip>
// don't forget to include appropriate headers

void write_stats(std::vector<double> const& data, std::ostream& out) {
    if (data.empty())
    {
        return;
    }
    auto min = std::min_element(data.begin(), data.end());
    auto max = std::max_element(data.begin(), data.end());
    auto mean = std::accumulate(data.begin(), data.end(), 0.0) / static_cast<double>(data.size());
    out << std::fixed << std::setprecision(2) << "min: " << *min << "\nmax: " << *max << "\nmean: " << mean << std::endl;
}
