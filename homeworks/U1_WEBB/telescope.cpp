#include "telescope.hpp"
#include <sstream>
#include <iomanip>
#include <regex>
#include <string>
#include <cmath>

std::pair<size_t, size_t> parse_matrix(std::istream& in)
{
    std::pair<int, int> result;

    int width = 0;
    int height = 0;

    std::regex expression("\\d[^\\S\\r\\n]+(?=[\\d-])");
    std::string currentLine;
    int currentLineWidth;

    while (std::getline(in, currentLine))
    {
        height++;

        currentLineWidth = 1 + std::distance(std::sregex_iterator(currentLine.begin(), currentLine.end(), expression), std::sregex_iterator());
        if (width != 0 && width != currentLineWidth)
        {
            throw std::invalid_argument("Inconsistent row size");
        }

        width = currentLineWidth;
    }

    result.first = height;
    result.second = width;
    return result;
}

std::vector<int> parse_matrix(std::istream& in, const std::pair<size_t, size_t>& m_size)
{
    std::vector<int> result;

    std::string stream(std::istreambuf_iterator<char>(in), {});
    std::regex expression("-?\\d+");
    std::smatch match;
    std::string currentNumber;

    std::istringstream is(stream);
    parse_matrix(is); // Throw exception on invalid matrix

    while (std::regex_search(stream, match, expression))
    {
        currentNumber = stream.substr(match.position(), match.length());
        stream = stream.substr(match.position() + match.length());

        result.push_back(std::stoi(currentNumber));
    }

    return result;
}

void print_matrix(std::ostream& out, const std::pair<size_t, size_t>& m_size, const std::vector<int>& vec)
{
    if (m_size.first == 0 || m_size.second == 0)
    {
        out << "";
        return;
    }

    if (m_size.first * m_size.second != vec.size())
    {
        throw std::invalid_argument("Number count and matrix dimensions don't match.");
    }

    int cellWidth = 0;
    int currentCellWidth;

    for (int cell : vec)
    {
        if ((currentCellWidth = std::to_string(cell).length()) > cellWidth)
        {
            cellWidth = currentCellWidth;
        }
    }

    int resultWidth = (cellWidth + 3) * m_size.second + 1;
    std::string result;

    int i;
    int j;
    int k;
    std::string current;
    for (i = 0; i < resultWidth; i++)
    {
        result += "-";
    }
    result += "\n";
    for (i = 0; i < m_size.first; i++)
    {
        for (j = 0; j < m_size.second; j++)
        {
            result += "| ";
            current = std::to_string(vec[m_size.second*i + j]);
            for (k = current.length(); k < cellWidth; k++)
            {
                result += " ";
            }
            result += current;
            result += " ";
        }
        result += "|\n";
    }
    for (i = 0; i < resultWidth; i++)
    {
        result += "-";
    }

    out << result << std::endl;
}

std::vector<unsigned char> parse_stream(std::istream& in, const std::pair<size_t, size_t>& m_size)
{
    std::string stream(std::istreambuf_iterator<char>(in), {});
    if (m_size.first * m_size.second != stream.length())
    {
        throw std::invalid_argument("Char count and matrix dimensions don't match.");
    }

    std::vector<unsigned char> result(stream.begin(), stream.end());

    return result;
}

void rotate_down(const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec)
{
    rotate_down(m_size, vec, 1);
}

void rotate_down(const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec, int step)
{
    std::vector<unsigned char> result;

    step = -step;

    if (step < 0)
    {
        step = -step;
        step = step % m_size.first;
        step = m_size.first - step;
    }
    else
    {
        step = step % m_size.first;
    }

    int i;
    int index;
    for (i = 0; i < m_size.first; i++)
    {
        index = ((i+step)% m_size.first) * m_size.second;
        result.insert(result.end(), vec.begin() + index, vec.begin() + index + m_size.second);
    }

    vec = result;
}

void rotate_right(const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec)
{
    rotate_right(m_size, vec, 1);
}

void rotate_right(const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec, int step)
{
    std::vector<unsigned char> result;

    if (step < 0)
    {
        step = -step;
        step = step % m_size.second;
        step = m_size.second - step;
    }
    else
    {
        step = step % m_size.second;
    }

    int i;
    int h;
    for (i = 0; i < m_size.first; i++)
    {
        h = (i+1)*m_size.second;
        result.insert(result.end(), vec.begin() + h - step, vec.begin() + h);
        result.insert(result.end(), vec.begin() + i*m_size.second, vec.begin() + h - step);
    }

    vec = result;
}

void swap_points(const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec, const Point& p1, const Point& p2)
{
    if (m_size.first * m_size.second != vec.size())
    {
        throw std::invalid_argument("Vector size and matrix dimensions don't match.");
    }

    if (p1.x < 0 || p1.x >= m_size.second || p1.y < 0 || p1.y >= m_size.first || p2.x < 0 || p2.x >= m_size.second || p2.y < 0 || p2.y >= m_size.first)
    {
        throw std::invalid_argument("Point coordinates are out of bounds.");
    }

    unsigned char temp = vec[p1.y * m_size.second + p1.x];
    vec[p1.y * m_size.second + p1.x] = vec[p2.y * m_size.second + p2.x];
    vec[p2.y * m_size.second + p2.x] = temp;
}

void swap_points(const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec, const Point& p1, const Point& p2, const Point& delta)
{
    if (m_size.first * m_size.second != vec.size())
    {
        throw std::invalid_argument("Vector size and matrix dimensions don't match.");
    }

    Point size(delta.x - 1, delta.y - 1);

    if (p1.x < 0 || p1.x + size.x >= m_size.second || p1.y < 0 || p1.y + size.y >= m_size.first
    || p2.x < 0 || p2.x + size.x >= m_size.second || p2.y < 0 || p2.y + size.y >= m_size.first
    || !(p1.y > p2.y + size.y || p1.x > p2.x + size.x || p2.y > p1.y + size.y || p2.x > p1.x + size.x))
    {
        throw std::invalid_argument("Rectangle coordinates are out of bounds or are overlapping.");
    }

    int i, j;
    unsigned char temp;
    for (i = 0; i < delta.x; i++)
    {
        for (j = 0; j < delta.y; j++)
        {
            temp = vec[p1.x + (p1.y + j) * m_size.second + i];
            vec[p1.x + (p1.y + j) * m_size.second + i] = vec[p2.x + (p2.y + j) * m_size.second + i];
            vec[p2.x + (p2.y + j) * m_size.second + i] = temp;
        }
    }
}

/**
 * Parse image decoding instruction and call appropriate function.
 * @param instruction
 */
void decode_switch(std::string instruction, const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec)
{
    std::smatch match;
    std::smatch numMatch;
    std::string pointCoords;
    std::vector<int> points;
    int i;

    if (instruction[0] == 'r')
    {
        if (instruction == "r")
        {
            rotate_right(m_size, vec);
        }
        else
        {
            rotate_right(m_size, vec, std::stoi(instruction.substr(2)));
        }
    } else if (instruction[0] == 'l')
    {
        if (instruction == "l")
        {
            rotate_right(m_size, vec, -1);
        }
        else
        {
            rotate_right(m_size, vec, 0 - std::stoi(instruction.substr(2)));
        }
    } else if (instruction[0] == 'd')
    {
        if (instruction == "d")
        {
            rotate_down(m_size, vec);
        }
        else
        {
            rotate_down(m_size, vec, std::stoi(instruction.substr(2)));
        }
    } else if (instruction[0] == 'u')
    {
        if (instruction == "u")
        {
            rotate_down(m_size, vec, -1);
        }
        else
        {
            rotate_down(m_size, vec, 0 - std::stoi(instruction.substr(2)));
        }
    }
    else if (instruction[0] == 's')
    {
        std::string reducedInstruction = instruction.substr(2);
        int parsedNum;
        int parsedNumLength;

        for (i = 0; i < 4; i++)
        {
            parsedNum = std::stoi(reducedInstruction);
            points.insert(points.end(), parsedNum);
            parsedNumLength = parsedNum > 0 ? (int) log10 ((double) parsedNum) + 2 : 2;
            if (parsedNumLength > reducedInstruction.length())
            {
                parsedNumLength--;
            }
            reducedInstruction = reducedInstruction.substr(parsedNumLength);
        }

        if (reducedInstruction.empty())
        {
            swap_points(m_size, vec, Point(points[0], points[1]), Point(points[2], points[3]));
        }
        else
        {
            for (i = 0; i < 2; i++)
            {
                parsedNum = std::stoi(reducedInstruction);
                points.insert(points.end(), parsedNum);
                parsedNumLength = parsedNum > 0 ? (int) log10 ((double) parsedNum) + 2 : 2;
                if (parsedNumLength > reducedInstruction.length())
                {
                    parsedNumLength--;
                }
                reducedInstruction = reducedInstruction.substr(parsedNumLength);
            }
            swap_points(m_size, vec, Point(points[0], points[1]), Point(points[2], points[3]), Point(points[4], points[5]));
        }


//        if (std::regex_search(instruction, match, std::regex("s( -?\\d+){4}$")))
//        {
////            pointCoords = instruction;
////            for (i = 0; i < 4; i++)
////            {
////                std::regex_search(pointCoords, numMatch, std::regex("\\d+"));
////                points.insert(points.end(), std::stoi(numMatch.str(0)));
////                pointCoords.erase(0, pointCoords.find(numMatch.str(0)) + numMatch.str(0).length());
////            }
////            swap_points(m_size, vec, Point(points[0], points[1]), Point(points[2], points[3]));
//        }
//        else
//        {
//            pointCoords = instruction;
//            for (i = 0; i < 6; i++)
//            {
//                std::regex_search(pointCoords, numMatch, std::regex("\\d+"));
//                points.insert(points.end(), std::stoi(numMatch.str(0)));
//                pointCoords.erase(0, pointCoords.find(numMatch.str(0)) + numMatch.str(0).length());
//            }
//            swap_points(m_size, vec, Point(points[0], points[1]), Point(points[2], points[3]), Point(points[4], points[5]));
//        }
    }

//    std::cout << "instruction: " << instruction << std::endl;

//    if (std::regex_search(instruction, match, std::regex("r(?! [\\d-])")))
//    {
////        std::cout << "PARSED: r" << std::endl;
//        rotate_right(m_size, vec);
//    } else if (std::regex_search(instruction, match, std::regex("r -?\\d+")))
//    {
////        std::cout << "PARSED: r cis" << std::endl;
//        rotate_right(m_size, vec, std::stoi(match.str(0).substr(2)));
//    } else if (std::regex_search(instruction, match, std::regex("l(?! [\\d-])")))
//    {
////        std::cout << "PARSED: l" << std::endl;
//        rotate_right(m_size, vec, -1);
//    } else if (std::regex_search(instruction, match, std::regex("l -?\\d+")))
//    {
////        std::cout << "PARSED: l cis" << std::endl;
//        rotate_right(m_size, vec, 0 - std::stoi(match.str(0).substr(2)));
//    } else if (std::regex_search(instruction, match, std::regex("d(?! [\\d-])")))
//    {
////        std::cout << "PARSED: d" << std::endl;
//        rotate_down(m_size, vec);
//    } else if (std::regex_search(instruction, match, std::regex("d -?\\d+")))
//    {
////        std::cout << "PARSED: d cis" << std::endl;
//        rotate_down(m_size, vec, std::stoi(match.str(0).substr(2)));
//    } else if (std::regex_search(instruction, match, std::regex("u(?! [\\d-])")))
//    {
////        std::cout << "PARSED: u" << std::endl;
//        rotate_down(m_size, vec, -1);
//    } else if (std::regex_search(instruction, match, std::regex("u -?\\d+")))
//    {
////        std::cout << "PARSED: u cis" << std::endl;
//        rotate_down(m_size, vec, 0 - std::stoi(match.str(0).substr(2)));
//    } else if (std::regex_search(instruction, match, std::regex("s( -?\\d+){4}$")))
//    {
////        std::cout << "PARSED: s 4num" << std::endl;
//        pointCoords = match.str(0);
//        for (i = 0; i < 4; i++)
//        {
//            std::regex_search(pointCoords, numMatch, std::regex("\\d+"));
//            points.insert(points.end(), std::stoi(numMatch.str(0)));
//            pointCoords.erase(0, pointCoords.find(numMatch.str(0)) + numMatch.str(0).length());
//        }
//        swap_points(m_size, vec, Point(points[0], points[1]), Point(points[2], points[3]));
//    } else if (std::regex_search(instruction, match, std::regex("s( -?\\d+){6}")))
//    {
////        std::cout << "PARSED: s 6num" << std::endl;
//        pointCoords = match.str(0);
//        for (i = 0; i < 6; i++)
//        {
//            std::regex_search(pointCoords, numMatch, std::regex("\\d+"));
//            points.insert(points.end(), std::stoi(numMatch.str(0)));
//            pointCoords.erase(0, pointCoords.find(numMatch.str(0)) + numMatch.str(0).length());
//        }
//        swap_points(m_size, vec, Point(points[0], points[1]), Point(points[2], points[3]), Point(points[4], points[5]));
//    }

//    std::cout << "match str: " << match.str(0) << std::endl;
//    std::cout << "match[o]: " << match[0] << "\n" << std::endl;
}

void decode_picture(const std::string& file, const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec)
{
    std::ifstream fileStream(file);
    std::string instruction;

    while (std::getline(fileStream, instruction))
    {
        decode_switch(instruction, m_size, vec);
    }
}
