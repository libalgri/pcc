#pragma once

#include <memory>
#include <cfloat>
#include <climits>

enum class generator_kind {
    random,
    mersenne,
    minstd
};


class numbers_generator {
protected:
    int boolsGenerated = 0;
    int truesGenerated = 0;
    int falsesGenerated = 0;
    int doublesGenerated = 0;
    int intsGenerated = 0;
    double maxDouble = DBL_MIN;
    double minDouble = DBL_MAX;
    int maxInt = INT_MIN;
    int minInt = INT_MAX;

public:
    /**
     * Vygeneruje náhodné číslo v intervalu [0, 1)
     */
    virtual double random_double() = 0;
    /**
     * Vygeneruje náhodné celé číslo v intervalu [lower, upper]
     */
    virtual int    random_int(int lower, int upper) = 0;
    /**
     * Vygeneruje náhodnou pravdivostní hodnotu (true/false)
     */
    virtual bool   random_bool() = 0;

    virtual ~numbers_generator() = default;

    static std::unique_ptr<numbers_generator> create_by_kind(generator_kind kind, size_t seed);

    double min_generated_double() const {return minDouble;}
    double max_generated_double() const {return maxDouble;}
    size_t doubles_generated() const {return doublesGenerated;}
    int min_generated_int() const {return minInt;}
    int max_generated_int() const {return maxInt;}
    size_t ints_generated() const {return intsGenerated;}
    size_t trues_generated() const {return truesGenerated;}
    size_t falses_generated() const {return falsesGenerated;}
    size_t bools_generated() const {return boolsGenerated;}
};
