#include "tiny-07.hpp"

#include <cstdint>

std::unique_ptr<numbers_generator> numbers_generator::clone()
{
    if (dynamic_cast<mersenne_twister_generator*>(this) != nullptr)
    {
        auto *s1 = dynamic_cast<mersenne_twister_generator*>(this);
        auto *g1 = new mersenne_twister_generator(*s1);
        return std::unique_ptr<numbers_generator>(g1);
    }
    else if (dynamic_cast<minstd_generator*>(this) != nullptr)
    {
        auto *s1 = dynamic_cast<minstd_generator*>(this);
        auto *g1 = new minstd_generator(*s1);
        return std::unique_ptr<numbers_generator>(g1);
    }
    else if (dynamic_cast<fixed_generator*>(this) != nullptr)
    {
        auto *s1 = dynamic_cast<fixed_generator*>(this);
        auto *g1 = new fixed_generator(*s1);
        return std::unique_ptr<numbers_generator>(g1);
    }

    return nullptr;
}

mersenne_twister_generator::mersenne_twister_generator(size_t seed):
    rng(seed)
{}

double mersenne_twister_generator::random_double() {
    return double_dist(rng);
}

int mersenne_twister_generator::random_int(int lower, int upper) {
    return uniform_int_distribution(lower, upper)(rng);
}

bool mersenne_twister_generator::random_bool() {
    return bool_dist(rng);
}

std::unique_ptr<mersenne_twister_generator> mersenne_twister_generator::clone()
{
    auto *generator = new mersenne_twister_generator(*this);
    return std::unique_ptr<mersenne_twister_generator>(generator);
}
std::unique_ptr<minstd_generator> minstd_generator::clone()
{
    auto *generator = new minstd_generator(*this);
    return std::unique_ptr<minstd_generator>(generator);
}
std::unique_ptr<fixed_generator> fixed_generator::clone()
{
    auto *generator = new fixed_generator(*this);
    return std::unique_ptr<fixed_generator>(generator);
}

minstd_generator::minstd_generator(size_t seed):
    rng(static_cast<uint32_t>(seed))
{}

double minstd_generator::random_double() {
    return double_dist(rng);
}

int minstd_generator::random_int(int lower, int upper) {
    return uniform_int_distribution(lower, upper)(rng);
}

bool minstd_generator::random_bool() {
    return bool_dist(rng);
}
