#include "crusoe.hpp"

#include <utility>
#include <string>
#include <algorithm>
#include <iostream>
#include <stack>
#include <queue>
#include <map>

vertex::vertex() = default;

vertex::vertex(std::string str, int x, int y, std::string col)
{
    xy.first = x;
    xy.second = y;
    name = std::move(str);
    c_forward = std::move(col);
    neighbours = {};
}

bool vertex::add_neighbour(size_t vv, const std::string &col)
{
    std::pair<size_t, std::string> neighbor = {vv, col};
    if(std::find(neighbours.begin(), neighbours.end(), neighbor) == neighbours.end())
    {
        neighbours.push_back(neighbor);
        return true;
    }
    return false;
}

std::vector<std::pair<size_t, std::string>> vertex::get_neighbour() const
{
    return neighbours;
}

std::pair<int, int> vertex::get_xy() const
{
    return xy;
}

void vertex::set_color(const std::string &col)
{
    c_forward = col;
}

std::string vertex::get_color() const
{
    return c_forward;
}

void vertex::set_edge_color(size_t vv, const std::string &col)
{
    for (std::pair<size_t, std::string> &n : neighbours)
    {
        if (n.first == vv)
        {
            n.second = col;
        }
    }
}

std::string vertex::get_edge_color(size_t vv)
{
    for (std::pair<size_t, std::string> &n : neighbours)
    {
        if (n.first == vv)
        {
            return n.second;
        }
    }
    return "#FFFFFF";
}

/** GRAPH */

void graph::add_vertex(int x, int y, const std::string &col)
{
    vertex v(std::to_string(num_elem), x, y, col);
    vertices.push_back(v);
    num_elem++;
}

bool vertex_exists(size_t vv, const graph& gg)
{
    return !gg.get_vertex(vv).get_color().empty();
}

void graph::add_edge(size_t v1, size_t v2, const std::string &col)
{
    vertices[v1].add_neighbour(v2, col);
    vertices[v2].add_neighbour(v1, col);
}

bool graph::is_edge(size_t v1, size_t v2) const
{
    std::vector<std::pair<size_t, std::string>> neighbours = get_vertex(v1).get_neighbour();
    for (std::pair<size_t, std::string> &n : neighbours) // possible optimization
    {
        std::cout << n.first;
        std::cout << v2;
        if (n.first == v2)
        {
            return true;
        }
    }
    return false;
}

std::string graph::edge_color(size_t v1, size_t v2) const
{
    if (!vertex_exists(v1, *this))
    {
        return "#FFFFFF";
    }
    return get_vertex(v1).get_edge_color(v2);
}

std::string graph::vertex_color(size_t v1) const
{
    if (!vertex_exists(v1, *this))
    {
        return "#FFFFFF";
    }
    return get_vertex(v1).get_color();
}

void graph::set_vertex_color(size_t v1, const std::string &col)
{
    if (v1 >= vertices.size())
    {
        return;
    }
    vertices[v1].set_color(col);
}

void graph::set_edge_color(size_t v1, size_t v2, const std::string &col)
{
    if (!vertex_exists(v1, *this) || !vertex_exists(v2, *this))
    {
        return;
    }
    vertices[v1].set_edge_color(v2, col);
    vertices[v2].set_edge_color(v1, col);
    int i= 0;
}

bool graph::empty() const
{
    return vertices.empty();
}

size_t graph::size() const
{
    return vertices.size();
}

size_t graph::num_edge() const
{
    size_t result = 0;
    for (const vertex &v : vertices)
    {
        result += v.get_neighbour().size();
    }
    return result/2;
}

// ------------------- stage 2 -------------------

vertex graph::get_vertex(size_t num) const
{
    if (num < vertices.size())
    {
        return vertices[num];
    }
    return {};
}

void graph::is_achievable(size_t from, std::vector<size_t> &achieved)
{
    achieved = {};
    std::stack<size_t> unexploredNeighbors;
    unexploredNeighbors.push(from);
    achieved.push_back(from);
    vertex current;
    while (!unexploredNeighbors.empty())
    {
        current = get_vertex(unexploredNeighbors.top());
        unexploredNeighbors.pop();
        for (const std::pair<size_t, std::string>& neighbor : current.get_neighbour())
        {
            if (std::find(achieved.begin(), achieved.end(), neighbor.first) == achieved.end())
            {
                unexploredNeighbors.push(neighbor.first);
                achieved.push_back(neighbor.first);
            }
        }
    }
}

void graph::color_component(std::vector<size_t> cmp, const std::string &col)
{
    vertex v;
    for (size_t vv : cmp)
    {
        v = get_vertex(vv);
        for (std::pair<size_t, std::string> &neighbor : v.get_neighbour())
        {
            set_edge_color(vv, neighbor.first, col);
        }
        set_vertex_color(vv, col);
    }
}

std::vector<size_t> graph::path(size_t v1, size_t v2)
{
    std::queue<size_t> queue = {};
    std::vector<size_t> explored = {v1};
    std::map<size_t, size_t> parent = {};
    queue.push(v1);
    size_t v;
    while (!queue.empty())
    {
        v = queue.front(); queue.pop();
        if (v == v2)
        {
            size_t current = v;
            std::vector<size_t> result;
            while (parent.count(current))
            {
                result.insert(result.begin(), current);
                current = parent[current];
            }
            result.insert(result.begin(), current);
            return result;
        }
        for (const std::pair<size_t, std::string>& pair : get_vertex(v).get_neighbour())
        {
            if (std::find(explored.begin(), explored.end(), pair.first) == explored.end())
            {
                explored.push_back(pair.first);
                parent.insert({pair.first, v});
                queue.push(pair.first);
            }
        }
    }
    return std::vector<size_t>{};
}

void graph::color_path(std::vector<size_t> pth, const std::string &col)
{
    for (int i = 0; i < pth.size()-1; i++)
    {
        set_edge_color(pth[i], pth[i+1], col);
    }
}

/** GRAPH_COMP*/

graph::graph_comp::graph_comp(graph &gg) : gg(gg)
{
    this->gg = gg;

    std::vector<size_t> unexplored = {};
    std::vector<size_t> achieved = {};
    for (int i = 0; i < gg.vertices.size(); i++)
    {
        unexplored.push_back(i);
    }
    while (!unexplored.empty())
    {
        gg.is_achievable(unexplored[0], achieved);
        for (size_t a : achieved)
        {
            unexplored.erase(std::remove(unexplored.begin(), unexplored.end(), a), unexplored.end());
        }
        components.push_back(achieved);
    }
}

void graph::graph_comp::color_componennts()
{
    std::vector<std::string> colors = {"red", "olive", "orange", "lightblue", "yellow", "pink", "cyan", "purple", "brown", "magenta"};
    unsigned long colorIndex = 0;
    unsigned long colorsSize = colors.size();
    for (const std::vector<size_t>& component : components)
    {
        if (component.size() == 1)
        {
            continue;
        }
        colorIndex = (colorIndex + 1) % colorsSize;
        gg.color_component(component, colors[colorIndex]);
    }
}

size_t graph::graph_comp::count() const
{
    return components.size();
}

size_t graph::graph_comp::count_without_one() const
{
    size_t result = 0;
    for (std::vector<size_t> component : components)
    {
        if (component.size() != 1)
        {
            result++;
        }
    }
    return result;
}

size_t graph::graph_comp::max_comp() const
{
    size_t biggest = 0;
    size_t biggestIndex = 0;

    for (int i = 0; i < components.size(); i++)
    {
        if (components[i].size() > biggest)
        {
            biggest = components[i].size();
            biggestIndex = i;
        }
    }

    return biggestIndex;
}

size_t graph::graph_comp::size_of_comp(size_t i) const
{
    return components[i].size();
}

std::vector<size_t> graph::graph_comp::get_component(size_t i) const
{
    return components[i];
}

bool graph::graph_comp::same_comp(size_t v1, size_t v2) const
{
    std::vector<size_t> component = {};
    gg.is_achievable(v1, component);
    return std::find(component.begin(), component.end(), v2) != component.end();
}

/** GRAPH FENCE*/

graph::graph_fence::graph_fence(graph &gg, size_t vv, size_t distance) : gg(gg)
{
    this->gg = gg;

    std::queue<size_t> queue = {};
    std::vector<size_t> explored = {vv};
    fence.push_back(vv);
    std::map<size_t, int> distances = {{vv, 0}};
    queue.push(vv);
    size_t current;
    while (!queue.empty())
    {
        current = queue.front(); queue.pop();
        if (distances[current] > 3)
        {
            continue;
        }
        for (const std::pair<size_t, std::string>& n : gg.get_vertex(vv).get_neighbour())
        {
            if (std::find(explored.begin(), explored.end(), n.first) == explored.end())
            {
                explored.push_back(n.first);
                fence.push_back(n.first);
                distances.insert({n.first, distances[current]+1});
            }
        }
    }
}

void graph::graph_fence::color_fence(const std::string &col)
{
    for (size_t v : fence)
    {
        gg.vertices[v].set_color(col);
    }
}

size_t graph::graph_fence::count_stake() const
{
    return fence.size();
}

size_t graph::graph_fence::get_stake(size_t i) const
{
    if (i >= count_stake())
    {
        return -1;
    }
    return fence[i];
}
