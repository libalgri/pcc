#include "trie.hpp"

#include <utility>
#include <algorithm>
#include <cassert>
#include <queue>
#include <array>
#include <stack>
#include <tuple>
#include <ostream>
#include <sstream>


trie::trie()
{

}

trie::trie(const std::vector<std::string>& strings)
{
    for (const std::string &s : strings)
    {
        insert(s);
    }
}

trie_node *get_child(trie_node *node, char c)
{
    return node->children[c];
}

std::vector<trie_node*> get_children(trie_node *node)
{
    std::vector<trie_node*> result = {};
    for (trie_node *n : node->children)
    {
        if (n != nullptr)
        {
            result.push_back(n);
        }
    }
    return result;
}

std::vector<trie_node*> get_all_descendants(trie_node *parent)
{
    if (parent == nullptr)
    {
        return std::vector<trie_node*>{};
    }
    std::vector<trie_node*> result = {parent};
    std::queue<trie_node*> unexplored = {};
    unexplored.push(parent);
    trie_node *current;
    while (!unexplored.empty())
    {
        current = unexplored.front(); unexplored.pop();
        for (trie_node *n : get_children(current))
        {
            unexplored.push(n);
            result.push_back(n);
        }
    }
    return result;
}

bool trie::erase(const std::string &str) // possible optimization: unlink nodes that aren't needed
{
    if (m_root == nullptr)
    {
        return false;
    }
    if (str.empty())
    {
        if (m_root->is_terminal)
        {
            m_root->is_terminal = false;
            if (get_children(m_root).empty())
            {
                delete m_root;
                m_root = nullptr;
            }
            return true;
        }
        return false;
    }
    trie_node *current = m_root;
    trie_node *next;
    for (const char &c : str)
    {
        next = get_child(current, c);
        if (next == nullptr)
        {
            return false;
        }
        current = next;
    }
    if (!current->is_terminal)
    {
        return false;
    }
    current->is_terminal = false;
    trie_node *old;
    for (unsigned long i = str.size()+1; i > 0; i--)
    {
        if (!current->is_terminal && get_children(current).empty())
        {
            old = current;
            if (current == m_root)
            {
                delete m_root;
                m_root = nullptr;
                break;
            }
            current = current->parent;
            current->children[str[i-2]] = nullptr;
            delete old;
        }
        else
        {
            break;
        }
    }
    return true;
}

bool trie::insert(const std::string &str)
{
    if (m_root == nullptr)
    {
        m_root = new trie_node();
    }
    if (str.empty())
    {
        if (m_root->is_terminal)
        {
            return false;
        }
        m_root->is_terminal = true;
        return true;
    }
    trie_node* current = m_root;
    trie_node *next;
    bool isNew = false;
    for (const char& c : str)
    {
        next = get_child(current, c);
        if (next != nullptr)
        {
            current = next;
        }
        else
        {
            next = new trie_node();
            next->payload = c;
            next->parent = current;
            current->children[c] = next;
            current = next;
            isNew = true;
        }
    }
    if (!current->is_terminal)
    {
        isNew = true;
        current->is_terminal = true;
    }
    return isNew;
}

bool trie::contains(const std::string &str) const
{
    if (m_root == nullptr)
    {
        return false;
    }
    if (str.empty())
    {
        return m_root->is_terminal;
    }
    trie_node *current = m_root;
    trie_node *next;
    for (const char &c : str)
    {
        next = get_child(current, c);
        if (next == nullptr)
        {
            return false;
        }
        current = next;
    }
    return current->is_terminal;
}

size_t trie::size() const
{
    if (m_root == nullptr)
    {
        return 0;
    }
    int result = m_root->is_terminal;
    std::queue<trie_node*> unexplored = {};
    unexplored.push(m_root);
    trie_node *current;
    while (!unexplored.empty())
    {
        current = unexplored.front(); unexplored.pop();
        for (trie_node *n : get_children(current))
        {
            if (n == nullptr)
            {
                continue;
            }
            result += n->is_terminal;
            unexplored.push(n);
        }
    }
    return result;
}

bool trie::empty() const
{
    return m_root == nullptr;
}

std::vector<std::string> trie::search_by_prefix(const std::string &prefix) const
{
    if (m_root == nullptr)
    {
        return {};
    }
    std::vector<std::string> result;
    trie_node* root = m_root;
    for (const char &c : prefix)
    {
        root = root->children[static_cast<unsigned char>(c)];
        if (root == nullptr)
        {
            return {};
        }
    }
    std::queue<std::tuple<trie_node*, std::string>> unexplored;
    unexplored.emplace(root, "");
    std::tuple<trie_node*, std::string> current;
    while (!unexplored.empty())
    {
        current = unexplored.front(); unexplored.pop();
        if (std::get<0>(current)->is_terminal)
        {
            result.push_back(prefix + std::get<1>(current));
        }
        for (trie_node* n : get_children(std::get<0>(current)))
        {
            unexplored.emplace(n, std::get<1>(current) + n->payload);
        }
    }
    return result;
}

std::vector<std::string> trie::get_prefixes(const std::string &str) const
{
    if (m_root == nullptr)
    {
        return {};
    }
    std::vector<std::string> result = {};
    trie_node* current = m_root;
    std::string prefix;
    if (current->is_terminal)
    {
        result.push_back(prefix);
    }
    for (const char &c : str)
    {
        current = current->children[static_cast<unsigned char>(c)];
        if (current == nullptr)
        {
            break;
        }
        prefix += current->payload;
        if (current->is_terminal)
        {
            result.push_back(prefix);
        }
    }
    return result;
}

void trie::swap(trie &rhs)
{
    trie_node* tmp = m_root;
    m_root = rhs.m_root;
    rhs.m_root = tmp;
}

bool trie::operator==(const trie &rhs) const
{
    if (rhs.m_root == nullptr || m_root == nullptr)
    {
        return m_root == rhs.m_root;
    }
    std::queue<std::array<trie_node*, 2>> unexplored;
    unexplored.push({rhs.m_root, m_root});
    std::array<trie_node*, 2> current = {};
    while (!unexplored.empty())
    {
        current = unexplored.front(); unexplored.pop();
        for (int i = 0; i < 128; i++)
        {
            if (current[0]->children[i] != nullptr || current[1]->children[i] != nullptr)
            {
                if (current[0]->children[i] == nullptr || current[1]->children[i] == nullptr)
                {
                    return false;
                }
                unexplored.push({current[0]->children[i], current[1]->children[i]});
            }
        }
    }
    return true;
}

bool trie::operator<(const trie &rhs) const
{
    if (m_root == nullptr || rhs.m_root == nullptr)
    {
        return rhs.m_root != nullptr;
    }
    std::stack<std::array<trie_node*, 2>> path;
    path.push({m_root, rhs.m_root});
    std::array<trie_node*, 2> current = {};
    int startingIndex = 0;
    while(!path.empty())
    {
        start:
        current = path.top();
        for (int i = startingIndex; i < 128; i++)
        {
            if (current[0]->children[i] != nullptr || current[1]->children[i] != nullptr)
            {
                if (current[0]->children[i] == nullptr)
                {
                    return true;
                }
                if (current[1]->children[i] == nullptr)
                {
                    return false;
                }
                path.push({current[0]->children[i], current[1]->children[i]});
                startingIndex = 0;
                goto start;
            }
        }
        startingIndex = static_cast<unsigned char>(current[0]->payload)+1;
        path.pop();
    }
    return false;
}

trie trie::operator&(const trie &rhs) const
{
    trie result = trie();
    if (empty() || rhs.empty())
    {
        return result;
    }
    result.m_root = new trie_node();
    std::queue<std::array<trie_node*, 3>> unexplored;
    unexplored.push({m_root, rhs.m_root, result.m_root});
    std::array<trie_node*, 3> current = {};
    trie_node* newNode;
    bool hasChildren;
    trie_node* removeNode;
    trie_node* removeNodeOld;
    while (!unexplored.empty())
    {
        current = unexplored.front(); unexplored.pop();
        current[2]->is_terminal = current[0]->is_terminal && current[1]->is_terminal;
        current[2]->payload = current[0]->payload;
        hasChildren = false;
        for (int i = 0; i < 128; i++)
        {
            if (current[0]->children[i] != nullptr && current[1]->children[i] != nullptr)
            {
                newNode = new trie_node();
                newNode->parent = current[2];
                current[2]->children[i] = newNode;
                unexplored.push({current[0]->children[i], current[1]->children[i], newNode});
                hasChildren = true;
            }
        }
        if (!hasChildren && !current[2]->is_terminal)
        {
            removeNode = current[2];
            while (removeNode->parent != nullptr)
            {
                if (removeNode->is_terminal || !get_children(removeNode).empty() || removeNode->payload == 0)
                {
                    break;
                }
                removeNodeOld = removeNode;
                removeNode = removeNode->parent;
                removeNode->children[removeNodeOld->payload] = nullptr;
                delete removeNodeOld;
            }
        }
    }
    if (!result.m_root->is_terminal && get_children(result.m_root).empty())
    {
        delete result.m_root;
        result.m_root = nullptr;
    }
    return result;
}

trie trie::operator|(const trie &rhs) const
{
    if (rhs.m_root == nullptr)
    {
        return *this;
    }
    trie result = trie();
    result.m_root = new trie_node();
    std::queue<std::array<trie_node*, 3>> unexplored;
    unexplored.push({m_root, rhs.m_root, result.m_root});
    std::array<trie_node*, 3> current = {};
    trie_node* newNode;
    trie_node* first;
    trie_node* second;
    while (!unexplored.empty())
    {
        current = unexplored.front(); unexplored.pop();
        if (current[0] != nullptr)
        {
            current[2]->is_terminal = current[0]->is_terminal;
            current[2]->payload = current[0]->payload;
        }
        else
        {
            current[2]->payload = current[1]->payload;
        }
        if (current[1] != nullptr)
        {
            current[2]->is_terminal = current[2]->is_terminal || current[1]->is_terminal;
        }
        for (int i = 0; i < 128; i++)
        {
            first = current[0] == nullptr ? nullptr : current[0]->children[i];
            second = current[1] == nullptr ? nullptr : current[1]->children[i];
            if (first != nullptr || second != nullptr)
            {
                newNode = new trie_node();
                current[2]->children[i] = newNode;
                newNode->parent = current[2];
                unexplored.push({first, second, newNode});
            }
        }
    }
    return result;
}

bool operator!=(const trie& lhs, const trie& rhs)
{
    return !(lhs == rhs);
}

bool operator<=(const trie& lhs, const trie& rhs)
{
    return lhs < rhs || lhs == rhs; // can be optimized
}

bool operator>(const trie& lhs, const trie& rhs)
{
    return !(lhs < rhs) && lhs != rhs; // can be optimized
}

bool operator>=(const trie& lhs, const trie& rhs)
{
    return lhs > rhs || lhs == rhs; // can be optimized
}

std::ostream& operator<<(std::ostream& out, trie const& trie)
{
    auto o = std::ostringstream("");
    return o;
}

trie::~trie()
{
    for (trie_node *n : get_all_descendants(m_root))
    {
        delete n;
    }
}

trie::trie(const trie &rhs)
{
    if (rhs.m_root == nullptr)
    {
        m_root = nullptr;
        return;
    }
    m_root = new trie_node();
    std::queue<std::array<trie_node*, 2>> unexplored;
    unexplored.push({rhs.m_root, m_root});
    std::array<trie_node*, 2> current = {};
    trie_node* newNode;
    while (!unexplored.empty())
    {
        current = unexplored.front(); unexplored.pop();
        for (trie_node* n : get_children(current[0]))
        {
            newNode = new trie_node();
            newNode->payload = n->payload;
            newNode->is_terminal = n->is_terminal;
            newNode->parent = current[1];
            current[1]->children[newNode->payload] = newNode;
            unexplored.push({n, newNode});
        }
    }
}

trie &trie::operator=(const trie &rhs) // possible optimization: preserve intersect from rhs
{
    trie t = trie(rhs);
    for (trie_node *n : get_all_descendants(m_root))
    {
        delete n;
    }
    m_root = t.m_root;
    t.m_root = nullptr;
    return *this;
}

trie::trie(trie &&rhs)
{
    m_root = rhs.m_root;
    rhs.m_root = nullptr;
}

trie &trie::operator=(trie &&rhs)
{
    for (trie_node *n : get_all_descendants(m_root))
    {
        delete n;
    }
    m_root = rhs.m_root;
    rhs.m_root = nullptr;
    return *this;
}

std::vector<trie_node*> get_word(trie_node *end)
{
    if (end == nullptr)
    {
        return {};
    }
    std::vector<trie_node*> result = {end};
    while (end->parent != nullptr)
    {
        end = end->parent;
        result.insert(result.begin(), end);
    }
    return result;
}

trie::const_iterator trie::begin() const
{
    if (m_root == nullptr)
    {
        return nullptr;
    }
    trie_node *current = m_root;
    std::vector<trie_node*> children = get_children(m_root);
    while (!children.empty())
    {
        if (current->is_terminal)
        {
            break;
        }
        current = children[0];
        children = get_children(current);
    }
    auto iterator = trie::const_iterator(current);
    return iterator;
}

trie::const_iterator trie::end() const
{
    return {};
}


trie::const_iterator::const_iterator(const trie_node *node)
{
    current_node = node;
}

trie::const_iterator &trie::const_iterator::operator++()
{
    const trie_node *current = current_node;
    std::vector<trie_node*> children = {};
    for (trie_node *n : current->children)
    {
        if (n != nullptr)
        {
            children.push_back(n);
        }
    }
    if (children.empty())
    {
        const trie_node *parent = nullptr;
        const trie_node *child = current;
        while (child->parent != nullptr)
        {
            parent = child->parent;
            for (int i = static_cast<unsigned char>(child->payload+1); i < 128; i++)
            {
                if (parent->children[i] != nullptr)
                {
                    current = parent->children[i];
                    goto search;
                }
            }
            child = parent;
        }
        current_node = nullptr;
        goto end;
    }
    else
    {
        current = children[0];
    }
    search:
    while (current != nullptr)
    {
        if (current->is_terminal)
        {
            break;
        }
        children = {};
        for (trie_node *n : current->children)
        {
            if (n != nullptr)
            {
                children.push_back(n);
            }
        }
        current = children[0];
    }
    current_node = current;

    end:
    return *this;
}

trie::const_iterator trie::const_iterator::operator++(int i)
{
    trie::const_iterator iterator = trie::const_iterator(current_node);
    ++(*this);
    return iterator;
}

trie::const_iterator::reference trie::const_iterator::operator*() const
{
    if (current_node == nullptr)
    {
        return "";
    }
    const trie_node *n = current_node;
    std::string result;
    while (n->parent != nullptr)
    {
        result.insert(0, 1, n->payload);
        n = n->parent;
    }
    return result;
}

bool trie::const_iterator::operator==(const trie::const_iterator &rhs) const
{
    return current_node == rhs.current_node;
}

bool trie::const_iterator::operator!=(const trie::const_iterator &rhs) const
{
    return !(*this == rhs);
}

