### Rozdělení repozitáře

Repozitář je rozdělen na dvě části: `homeworks` a `tetris`.

- `homeworks` obsahuje domácí úkoly z průběhu semestru.
- `tetris` obsahuje semestrální práci, hru Tetris, včetně dokumentace v podobě README souboru.
