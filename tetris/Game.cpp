//
// Created by Gek on 11/10/2023.
//

#include <iostream>
#include <algorithm>
#include <termios.h>
#include "Game.h"
#include "controls/KeyPressManager.h"

/**
 * Initialize Game components.
 */
Game::Game()
{
    playingField = new PlayingField(this);
    queue = new Queue(5, this);
    graphics = new GameGraphics(this);
    hold = new Hold(this);
    controlsManager = new ControlsManager(this);
    timer = new Timer(this);
    level = new Level();
    score = new Score(this);
}

Game::~Game()
{
    delete playingField;
    delete queue;
    delete graphics;
    delete hold;
    delete controlsManager;
    delete timer;
    delete level;
    delete score;
}

/**
 * Prompt user to select the starting level.
 * Start the Timer.
 */
void Game::start()
{
    std::string startingLevel = "0";
    while (!std::all_of(startingLevel.begin(), startingLevel.end(), ::isdigit) || std::stoi(startingLevel) <= 0)
    {
        std::cout << "Enter level to start on (must be 1 or greater):" << std::endl;
        std::cin >> startingLevel;
    }
    KeyPressManager::disableEchoing();
    level->setCurrentLevel(std::stoi(startingLevel));
    queue->shiftQueue();
    timer->start();
}

/**
 * Stop the Timer and disable echoing in the console.
 */
void Game::stop()
{
    timer->stop();
    KeyPressManager::enableEchoing();
}

/**
 * Stop the Timer and color all pieces as gray.
 */
void Game::gameOver()
{
    stop();
    graphics->setAllPiecesGray(true);
    graphics->drawPlayingField();
}

PlayingField *Game::getPlayingField()
{
    return playingField;
}

Queue *Game::getQueue()
{
    return queue;
}

GameGraphics *Game::getGraphics()
{
    return graphics;
}

Hold *Game::getHold()
{
    return hold;
}

Timer *Game::getTimer()
{
    return timer;
}

ControlsManager *Game::getControlsManager()
{
    return controlsManager;
}

Level *Game::getLevel()
{
    return level;
}

Score *Game::getScore()
{
    return score;
}
