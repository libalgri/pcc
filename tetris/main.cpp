#include "Game.h"
#include "game-logic/PieceMovement.h"

/**
 * Start a new game.
 *
 * @return
 */
int main()
{
    Game game{};
    game.start();
    return 0;
}
