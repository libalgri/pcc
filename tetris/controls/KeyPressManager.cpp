//
// Created by Gek on 15/11/2023.
//

#include <unistd.h>
#include <termios.h>
#include <iostream>
#include "KeyPressManager.h"

/**
 * Turn characters from console into a set of instructions for the game.
 *
 * @param input Vector containing characters entered by the user.
 * @return A set of Controls to be processed.
 */
std::vector<Controls> KeyPressManager::decodeInput(const std::vector<char> &input)
{
    std::vector<Controls> result;
    int pos = 0;
    while (pos < input.size())
    {
        switch (input[pos])
        {
            case ' ':
                result.push_back(CONTROLS_HARD_DROP);
                break;
            case 'x':
            case 'X':
                result.push_back(CONTROLS_CLOCKWISE_ROTATE);
                break;
            case 'z':
            case 'Z':
            case 'y':
            case 'Y':
                result.push_back(CONTROLS_COUNTER_CLOCKWISE_ROTATE);
                break;
            case 'c':
            case 'C':
                result.push_back(CONTROLS_HOLD);
                break;
            case 'j':
            case 'J':
                result.push_back(CONTROLS_MOVE_LEFT);
                break;
            case 'k':
            case 'K':
                result.push_back(CONTROLS_SOFT_DROP);
                break;
            case 'l':
            case 'L':
                result.push_back(CONTROLS_MOVE_RIGHT);
                break;
        }
        pos++;
    }
    return result;
}

/**
 * Read input from the console and convert it into game instructions.
 *
 * @return A set of controls to be processed
 */
std::vector<Controls> KeyPressManager::getInput()
{
    std::vector<char> input = {};
    char buf = 0;

    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;

    while (select(1, &fds, NULL, NULL, &timeout) > 0 && FD_ISSET(0, &fds))
    {
        if (read(0, &buf, 1) < 0)
            perror ("read()");
        input.push_back(buf);
    }
    return decodeInput(input);
}

/**
 * Enable canonical mode and echoing in terminal.
 */
void KeyPressManager::enableEchoing()
{
    struct termios old = {0};

    if (tcgetattr(0, &old) < 0)
        perror("tcgetattr()");

    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;

    if (tcsetattr(0, TCSANOW, &old) < 0)
        perror("tcsetattr ICANON");
}

/**
 * Disable echoing and canonical mode.
 */
void KeyPressManager::disableEchoing()
{
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0)
        perror("tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
        perror("tcsetattr ICANON");
}


