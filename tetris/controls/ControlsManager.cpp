//
// Created by Gek on 15/11/2023.
//

#include <iostream>
#include "ControlsManager.h"
#include "../Game.h"
#include "KeyPressManager.h"

ControlsManager::ControlsManager(Game *game)
{
    this->game = game;
}

void ControlsManager::leftDown()
{
    pressingLeft = true;
}

void ControlsManager::leftUp()
{
    pressingLeft = false;
}

void ControlsManager::rightDown()
{
    pressingRight = true;
}

void ControlsManager::rightUp()
{
    pressingRight = false;
}

void ControlsManager::softDropDown()
{
    softDropping = true;
}
void ControlsManager::softDropUp()
{
    softDropping = false;
}

void ControlsManager::rotateClockwiseDown()
{
    rotatingClockwise = true;
}
void ControlsManager::rotateClockwiseUp()
{
    rotatingClockwise = false;
}

void ControlsManager::rotateCounterClockwiseDown()
{
    rotatingCounterClockwise = true;
}
void ControlsManager::rotateCounterClockwiseUp()
{
    rotatingCounterClockwise = false;
}

void ControlsManager::hardDropDown()
{
    hardDropping = true;
}
void ControlsManager::hardDropUp()
{
    hardDropping = false;
}

void ControlsManager::holdDown()
{
    holding = true;
}
void ControlsManager::holdUp()
{
    holding = false;
}

bool ControlsManager::isPressingLeft() const
{
    return pressingLeft;
}

bool ControlsManager::isPressingRight() const
{
    return pressingRight;
}

bool ControlsManager::isSoftDropping() const
{
    return softDropping;
}

bool ControlsManager::isHardDropping() const
{
    return hardDropping;
}

bool ControlsManager::isRotatingClockwise() const
{
    return rotatingClockwise;
}

bool ControlsManager::isRotatingCounterClockwise() const
{
    return rotatingCounterClockwise;
}

bool ControlsManager::isHolding() const
{
    return holding;
}

/**
 * Process controls entered by user by running the corresponding function to each control.
 */
void ControlsManager::getInstruction()
{
    std::vector<Controls> input =  KeyPressManager::getInput();
    for (Controls c : input)
    {
        switch (c)
        {
            case CONTROLS_MOVE_LEFT:
                leftDown();
                break;
            case CONTROLS_MOVE_RIGHT:
                rightDown();
                break;
            case CONTROLS_HARD_DROP:
                hardDropDown();
                break;
            case CONTROLS_SOFT_DROP:
                softDropDown();
                break;
            case CONTROLS_CLOCKWISE_ROTATE:
                rotateClockwiseDown();
                break;
            case CONTROLS_COUNTER_CLOCKWISE_ROTATE:
                rotateCounterClockwiseDown();
                break;
            case CONTROLS_HOLD:
                holdDown();
                break;
        }
    }
}
