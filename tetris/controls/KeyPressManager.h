//
// Created by Gek on 15/11/2023.
//

#ifndef TETRIS_KEYPRESSMANAGER_H
#define TETRIS_KEYPRESSMANAGER_H

#include <vector>

/**
 * Game instructions a player can trigger.
 */
enum Controls {
    CONTROLS_MOVE_LEFT,
    CONTROLS_MOVE_RIGHT,
    CONTROLS_SOFT_DROP,
    CONTROLS_CLOCKWISE_ROTATE,
    CONTROLS_COUNTER_CLOCKWISE_ROTATE,
    CONTROLS_HOLD,
    CONTROLS_HARD_DROP,
};

/**
 * Detector of user input in the terminal.
 */
class KeyPressManager
{
private:
    static std::vector<Controls> decodeInput(const std::vector<char>&);
public:
    static std::vector<Controls> getInput();
    static void enableEchoing();
    static void disableEchoing();
};


#endif //TETRIS_KEYPRESSMANAGER_H
