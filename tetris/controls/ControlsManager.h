//
// Created by Gek on 15/11/2023.
//

#ifndef TETRIS_CONTROLSMANAGER_H
#define TETRIS_CONTROLSMANAGER_H

class Game;

/**
 * Translator of user input to game instructions.
 */
class ControlsManager
{
private:
    bool pressingLeft = false;
    bool pressingRight = false;
    bool softDropping = false;
    bool hardDropping = false;
    bool rotatingClockwise = false;
    bool rotatingCounterClockwise = false;
    bool holding = false;

public:
    explicit ControlsManager(Game*);
    Game *game;
    void leftDown();
    void leftUp();
    void rightDown();
    void rightUp();
    void softDropDown();
    void softDropUp();
    void hardDropDown();
    void hardDropUp();
    void holdDown();
    void holdUp();
    void rotateClockwiseDown();
    void rotateClockwiseUp();
    void rotateCounterClockwiseDown();
    void rotateCounterClockwiseUp();

    void getInstruction();

    [[nodiscard]] bool isPressingLeft() const;
    [[nodiscard]] bool isPressingRight() const;
    [[nodiscard]] bool isSoftDropping() const;
    [[nodiscard]] bool isHardDropping() const;
    [[nodiscard]] bool isRotatingClockwise() const;
    [[nodiscard]] bool isRotatingCounterClockwise() const;
    [[nodiscard]] bool isHolding() const;
};


#endif //TETRIS_CONTROLSMANAGER_H
