//
// Created by Gek on 11/10/2023.
//

#ifndef TETRIS_GAME_H
#define TETRIS_GAME_H

#include "game-components/PlayingField.h"
#include "game-components/Queue.h"
#include "graphics/GameGraphics.h"
#include "game-components/Hold.h"
#include "game-logic/Timer.h"
#include "controls/ControlsManager.h"
#include "game-logic/Level.h"
#include "game-logic/Score.h"

/**
 * Class containing methods to start and stop the game.
 * This class also has access to most classes through getters.
 */
class Game
{
private:
    PlayingField *playingField;
    Queue *queue;
    GameGraphics *graphics;
    Hold *hold;
    Timer *timer;
    ControlsManager *controlsManager;
    Level *level;
    Score *score;
public:
    Game();
    ~Game();
    void start();
    void stop();
    void gameOver();

    PlayingField *getPlayingField();
    Queue *getQueue();
    GameGraphics *getGraphics();
    Hold *getHold();
    Timer *getTimer();
    ControlsManager *getControlsManager();
    Level *getLevel();
    Score *getScore();
};


#endif //TETRIS_GAME_H
