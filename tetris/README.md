# Semestrální praće z PCC

## Zadání

### Hra
Jako zadání jsem si vybral tvorbu počítačové hry Tetris.

### Tetris Guideline
Hra jsem vytvořil co nejvíce v souladu s [Tetris Guideline](https://tetris.wiki/Tetris_Guideline), specifikací používanou oficiálními verzemi Tetrisu. Oproti [klasickému tetrisu](https://tetris.wiki/Tetris_(NES,_Nintendo)) mají Tetris Guideline velmi významné rozdíly.

#### Generace dílků
Dílky (tetromina), se kterými hráč hraje nejsou generovány zcela náhodně. Tetris využívá tzv. [7-Bag system](https://tetris.wiki/Random_Generator), kde místo náhodně generované posloupnosti dílků se generují dílky po sedmi náhodně zamíchané mezi sebou. Tento systém garantuje získání jakéhokoliv typu dílku jednou za maximálně 13 dílků.

V této implementaci je vždy vidět příštích 5 takto vygenerovaných dílků.

#### Rotace dílků
Hra využívá tzv. [Super Rotation System](https://tetris.wiki/Super_Rotation_System). Tento systém obohacuje rotace o neobvyklé způsoby rotace dílků. Když hra zjistí, že "normální" rotace nelze provést, hledá alternativní pozice pro daný dílek. Alternativních pozic je celkem 4 pro každou kombinaci dílku a rotace. Tyto alternativní rotace zlepšují hratelnost ve většině situací, ale zároveň tvoří neintuitivní případy rotace, jako je např. [T-Spin Triple](https://four.lol/methods/tspin-triple).

#### Stínový dílek
Dílek který je ovládán hráčem má tzv. stínový dílek, který šedou barvou označuje pozici, kde by byl dílek položen
v případě, že hráč s dílkem už nebude manipulovat.

#### Hold slot
Nalevo od herního pole se nechází tzv. Hold slot, kam lze "schovat" dílek na jindy.
Při použití hold slotu se aktuální dílek dočasně přesune mimo hru a do hry přichází dílek, který
byl v hold slotu dříve.

## Implementace

Kód hry je rozdělen na několik částí které mezi sebou komunikují:
- Ovládání hry, definováno v adresáři `controls`
- Reprezentace komponent hry, jako jsou [tetromina](https://tetris.wiki/Tetromino) nebo samotná hrací plocha, definováno v adresáři `game-components`
- Funkčnost hry, definováno v adresáři `game-logic`
- Grafika, definována v adresáři `graphics`
- Třída `Game`, přes kterou se ovládá začátek a konec hry.

Každá třída a metoda (mimo gettery a settery) je dokumentována komentářema v kódu.

## Funkčnost a ovládání

### Začátek hry

Po spuštění programu je uživatel vyzván k vybrání úrovně hry.
Nejnižší úroveň je 1. S každou úrovní se zvyšuje rychlost hry až do úrovně 25, kde se rychlost už nezvyšuje,
ale hra je velmi obtížná na hraní. Úroveň hry taktéž ovlivňuje skóre získané během hry.

Po zadání začáteční úrovně začne hra.

### Ovládání hry

- Klávesy **J** a **L** posouvají dílek doleva a doprava
- **K** posouvá dílek směrem dolů
- **Mezerník** okamžitě položí dílek na místo stínového dílku
- **Z** a **X** (**Z** a **Y** na české klávesnici) rotují dílek proti a po směru hodinových ručiček
- **C** použije Hold slot (funkce popsána v sekci Zadání -> Hra -> Hold slot)

## Výsledek běhu programu

Následující odstavce popisují konkrétní situace a očekávané výsledky.

### Správná generace dílků

Situace: Uživatel spustí hru. Zadá do terminálu 1 jako výběr začáteční úrovně. Krátce poté zmáčkne klávesu **C**.

Očekávaný výsledek: Na displeji by měl být vidět právě jeden od každého ze sedmi různých druhů dílků.

![Obrázek očekávaného výsledku](./_README_images/ex1.png)

### Správná funkčnost rychlosti úrovní hry

Situace 1: Uživatel spustí hru. Zadá do terminálu **15** jako výběr začáteční úrovně a počká než program sám doběhne.

Očekávaný výsledek: Po cca 13 sekundách se hrací pole zaplní dílky a hra sama od sebe skončí, což se projeví zešednutím dílků na hracím poli.

![Obrázek očekávaného výsledku](./_README_images/ex2.png)

Situace 2: Uživatel spustí hru. Zadá do terminálu **25** jako výběr začáteční úrovně a počká než program sám doběhne.

Očekávaný výsledek: Po 1-2 sekundách se hrací pole zaplní dílky a hra sama od sebe skončí, což se projeví zešednutím dílků na hracím poli.

![Obrázek očekávaného výsledku](./_README_images/ex3.png)

### Správné počítání skóre

Situace 1: Uživatel spustí hru. Zadá do terminálu **5** jako výběr začáteční úrovně.
Uživatel položí 3 nebo 4 dílky takovým způsobem, že se zaplní a vyčistí jeden řádek hracího pole.

Očekávaný výsledek: Ukazatel skóre ukazuje hodnotu **500**.

![Obrázek očekávaného výsledku](./_README_images/ex4.png)

Situace 2: Uživatel spustí hru. Zadá do terminálu **5** jako výběr začáteční úrovně.
Uživatel položí kostičky takovým způsobem, aby vyčistil dva řádky najednou pomocí [T-Spinu](https://tetris.wiki/T-Spin) (tzv. T-Spin Double).

Očekávaný výsledek: Ukazatel skóre ukazuje hodnotu **6000**.

![Obrázek předcházející očekávaný výsledek](./_README_images/ex5_1.png)
![Obrázek předcházející očekávaný výsledek](./_README_images/ex5_2.png)
![Obrázek očekávaného výsledku](./_README_images/ex5_3.png)