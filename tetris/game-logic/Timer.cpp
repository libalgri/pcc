//
// Created by Gek on 15/11/2023.
//

#include "Timer.h"
#include "../Game.h"
#include "PieceMovement.h"
#include <thread>

Timer::Timer(Game *game)
{
    this->game = game;
    pieceMovement = new PieceMovement(game, game->getControlsManager());
}

Timer::~Timer()
{
    delete pieceMovement;
}

/**
 * Detect player input, process game events and render the playing field.
 */
void Timer::frame()
{
    pieceMovement->getControlsManager()->getInstruction();
    pieceMovement->doAllMovement();
    game->getGraphics()->drawPlayingField();
}

/**
 * Run frame() 60 times per second.
 */
void Timer::start()
{
    running = true;
    while (running)
    {
        frame();
        std::this_thread::sleep_for(std::chrono::milliseconds(frameTime));
    }
}

/**
 * Stop the timer.
 */
void Timer::stop()
{
    running = false;
}

/**
 * Freeze the game for certain amount of time.
 *
 * @param milliseconds Time in milliseconds.
 */
void Timer::pause(int milliseconds)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

int Timer::getFrameTime() const
{
    return frameTime;
}

PieceMovement *Timer::getPieceMovement()
{
    return pieceMovement;
}

void Timer::setFrameTime(int time)
{
    frameTime = time;
}
