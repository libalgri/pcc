//
// Created by Gek on 15/11/2023.
//

#ifndef TETRIS_PIECEMOVEMENT_H
#define TETRIS_PIECEMOVEMENT_H

class Game;
class ControlsManager;

/**
 * A system performing all kinds of in-game piece movement.
 */
class PieceMovement
{
private:
    Game *game;
    ControlsManager *controlsManager;

    int gravityCharge = 0;
    int lockTimeCharge = 0;
    int longLockTimeCharge = 0;
    bool hasTouchedGround = false;

    void horizontalMovement();
    void gravity();
    void manualDrop();
    void rotation();
    void hold();
    void lockTime();
public:
    explicit PieceMovement(Game*, ControlsManager*);

    void doAllMovement();
    void newPiece();
    ControlsManager* getControlsManager();
};


#endif //TETRIS_PIECEMOVEMENT_H
