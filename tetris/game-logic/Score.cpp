//
// Created by gek on 4.1.24.
//

#include "Score.h"
#include "../Game.h"

Score::Score(Game* game)
{
    this->game = game;
}

/**
 * Calculate score based on multiple factors such as how many lines have been cleared at once.
 *
 * @param linesCleared number of lines cleared at once.
 * @param isTSpin true if the action counts as a T Spin according to guideline Tetris rules.
 * @param isTSpinMini true if the action counts as a T Spin Mini according to guideline Tetris rules.
 */
void Score::calculateScore(int linesCleared, bool isTSpin, bool isTSpinMini)
{
    int baseScore = 0;
    if (isTSpin)
    {
        switch (linesCleared)
        {
            case 0:
                baseScore = isTSpinMini ? 100 : 400;
                break;
            case 1:
                baseScore = isTSpinMini ? 200 : 800;
                break;
            case 2:
                baseScore = 1200;
                break;
            case 3:
                baseScore = 1600;
                break;
        }
    } else
    {
        switch (linesCleared)
        {
            case 1:
                baseScore = 100;
                break;
            case 2:
                baseScore = 300;
                break;
            case 3:
                baseScore = 500;
                break;
            case 4:
                baseScore = 800;
                break;
        }
    }
    score += baseScore * game->getLevel()->getScoreMultiplier();
}

int Score::getScore() const
{
    return score;
}
