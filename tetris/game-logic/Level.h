//
// Created by gek on 3.1.24.
//

#ifndef TETRIS_LEVEL_H
#define TETRIS_LEVEL_H

/**
 * A class that keeps track of the in-game level and changes that happen when reaching new levels.
 */
class Level
{
private:
    int currentLevel{};
    int clearedLineCount{};
    double gravityTime{};
    int lockDelayTime{}; // in milliseconds
    int longLockDelayTime{};
    int scoreMultiplier{};

    void calculateLevelParameters();
public:
    Level();

    [[nodiscard]] double getGravityTime() const;
    [[nodiscard]] int getLockDelayTime() const;
    [[nodiscard]] int getLongLockDelayTime() const;
    [[nodiscard]] int getScoreMultiplier() const;
    [[nodiscard]] int getClearedLineCount() const;
    [[nodiscard]] int getCurrentLevel() const;

    void setCurrentLevel(int);
    void addClearedLines(int);
};


#endif //TETRIS_LEVEL_H
