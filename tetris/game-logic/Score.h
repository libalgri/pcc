//
// Created by gek on 4.1.24.
//

#ifndef TETRIS_SCORE_H
#define TETRIS_SCORE_H

class Game;

/**
 * A class to keep track of in-game score.
 */
class Score
{
private:
    int score = 0;
    Game* game;

public:
    explicit Score(Game*);
    void calculateScore(int, bool, bool);
    [[nodiscard]] int getScore() const;
};


#endif //TETRIS_SCORE_H
