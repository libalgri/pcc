//
// Created by Gek on 15/11/2023.
//

#ifndef TETRIS_TIMER_H
#define TETRIS_TIMER_H

class Game;
class PieceMovement;

/**
 * A timer loop that executes code at a given framerate.
 */
class Timer
{
private:
    Game *game;
    PieceMovement *pieceMovement;
    bool running = false;
    int frameTime = 17;

    void frame();
public:
    explicit Timer(Game*);
    ~Timer();

    void start();
    void stop();
    static void pause(int);

    [[nodiscard]] int getFrameTime() const;
    PieceMovement *getPieceMovement();

    void setFrameTime(int);
};


#endif //TETRIS_TIMER_H
