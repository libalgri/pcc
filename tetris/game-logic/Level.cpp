//
// Created by gek on 3.1.24.
//

#include <cmath>
#include "Level.h"

Level::Level()
= default;

/**
 * Calculate gravity, lock delay times and score multiplier based on the current level.
 */
void Level::calculateLevelParameters()
{
    if (currentLevel <= 15)
    {
        gravityTime = std::pow(0.8-((currentLevel-1)*0.007), currentLevel-1)*1000;
        lockDelayTime = 1000;
        longLockDelayTime = 5000;
    }
    else
    {
        gravityTime = 0;
        lockDelayTime = currentLevel < 25 ? 1000 - ((currentLevel-15)*100) : 100;
        longLockDelayTime = currentLevel < 25 ? 5000 - ((currentLevel-15)*500) : 1000;
    }
    scoreMultiplier = currentLevel;
}

double Level::getGravityTime() const
{
    return gravityTime;
}

int Level::getLockDelayTime() const
{
    return lockDelayTime;
}

int Level::getLongLockDelayTime() const
{
    return longLockDelayTime;
}

int Level::getScoreMultiplier() const
{
    return scoreMultiplier;
}

int Level::getClearedLineCount() const
{
    return clearedLineCount;
}

int Level::getCurrentLevel() const
{
    return currentLevel;
}

/**
 * Increment the cleared line counter.
 * Level is equal to line count divided by 10 + 1.
 * If starting level is greater than corresponding level, use the starting level.
 *
 * @param count Increment value.
 */
void Level::addClearedLines(int count)
{
    clearedLineCount += count;
    int newLevel = clearedLineCount / 10 + 1;
    if (newLevel > currentLevel)
    {
        currentLevel = newLevel;
    }
    scoreMultiplier = currentLevel;
    calculateLevelParameters();
}

void Level::setCurrentLevel(int level)
{
    currentLevel = level;
    calculateLevelParameters();
}




