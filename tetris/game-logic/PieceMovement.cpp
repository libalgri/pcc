//
// Created by Gek on 15/11/2023.
//

#include <iostream>
#include "PieceMovement.h"
#include "../controls/ControlsManager.h"
#include "../Game.h"

PieceMovement::PieceMovement(Game *game, ControlsManager *controlsManager)
{
    this->game = game;
    this->controlsManager = controlsManager;
}

/**
 * Move piece in all necessary ways.
 */
void PieceMovement::doAllMovement()
{
    lockTime();
    horizontalMovement();
    manualDrop();
    gravity();
    rotation();
    hold();
}

/**
 * Determines the direction to move the current piece in based on
 * currently active controls.
 */
void PieceMovement::horizontalMovement()
{
    if (controlsManager->isPressingLeft())
    {
        if (game->getPlayingField()->getCurrentPiece()->move(-1, 0))
        {
            lockTimeCharge = 0;
        }
        controlsManager->leftUp();
    }
    if (controlsManager->isPressingRight())
    {
        if (game->getPlayingField()->getCurrentPiece()->move(1, 0))
        {
            lockTimeCharge = 0;
        }
        controlsManager->rightUp();
    }
}

/**
 * Control automatic vertical piece movement determined by game gravity settings.
 */
void PieceMovement::gravity()
{
    if (game->getLevel()->getGravityTime() == 0)
    {
        game->getPlayingField()->getCurrentPiece()->moveRepeated(0, -1);
        return;
    }
    gravityCharge += game->getTimer()->getFrameTime();
    double gravityMovement = gravityCharge / game->getLevel()->getGravityTime();
    while (gravityMovement >= 1)
    {
        game->getPlayingField()->getCurrentPiece()->move(0, -1);
        gravityMovement--;
        gravityCharge = 0;
    }
}

/**
 * Execute soft drop and hard drop controls if pressed by the player.
 */
void PieceMovement::manualDrop()
{
    if (controlsManager->isHardDropping())
    {
        game->getPlayingField()->getCurrentPiece()->moveRepeated(0, -1);
        game->getPlayingField()->getCurrentPiece()->placeSelf();
        newPiece();
        controlsManager->hardDropUp();
        return;
    }
    if (controlsManager->isSoftDropping())
    {
        game->getPlayingField()->getCurrentPiece()->move(0, -1);
        controlsManager->softDropUp();
    }
}

/**
 * Execute rotation controls if pressed by the player.
 */
void PieceMovement::rotation()
{
    if (controlsManager->isRotatingClockwise())
    {
        if (game->getPlayingField()->getCurrentPiece()->rotate(CLOCKWISE))
        {
            lockTimeCharge = 0;
        }
        controlsManager->rotateClockwiseUp();
    }
    if (controlsManager->isRotatingCounterClockwise())
    {
        if (game->getPlayingField()->getCurrentPiece()->rotate(COUNTER_CLOCKWISE))
        {
            lockTimeCharge = 0;
        }
        controlsManager->rotateCounterClockwiseUp();
    }
}

/**
 * Hold piece if player pressed hold key.
 */
void PieceMovement::hold()
{
    if (controlsManager->isHolding())
    {
        game->getPlayingField()->getCurrentPiece()->holdSelf();
        controlsManager->holdUp();
        newPiece();
    }
}

/**
 * Determine if piece should lock down and be automatically placed.
 *
 * lockTimeCharge measures how long the piece has been touching ground without moving or getting rotated.
 * longLockTimeCharge measures time since the piece first touched a surface, regardless of it being moved around.
 */
void PieceMovement::lockTime()
{
    if (!game->getPlayingField()->getCurrentPiece()->move(0, -1))
    {
        hasTouchedGround = true;
        lockTimeCharge += game->getTimer()->getFrameTime();
        if (lockTimeCharge > game->getLevel()->getLockDelayTime() || longLockTimeCharge > game->getLevel()->getLongLockDelayTime())
        {
            game->getPlayingField()->getCurrentPiece()->placeSelf();
            newPiece();
        }
    }
    else
    {
        game->getPlayingField()->getCurrentPiece()->move(0, 1);
    }
    if (hasTouchedGround)
    {
        longLockTimeCharge += game->getTimer()->getFrameTime();
    }
}

/**
 * Reset all important values for piece movement.
 */
void PieceMovement::newPiece()
{
    gravityCharge = 0;
    lockTimeCharge = 0;
    longLockTimeCharge = 0;
    hasTouchedGround = false;
}

ControlsManager* PieceMovement::getControlsManager()
{
    return controlsManager;
}
