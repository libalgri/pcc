//
// Created by gek on 13.11.23.
//

#ifndef TETRIS_HOLD_H
#define TETRIS_HOLD_H

#include "piece-tables/PieceType.h"

class Piece;
class Game;

/**
 * The slot where the player can "save" a piece for later.
 */
class Hold
{
private:
    Game *game;
    Type heldPieceType = EMPTY;
    bool alreadyHeld = false;

public:
    explicit Hold(Game*);
    void hold(Piece*);
    Type getHeldPieceType();

    void setAlreadyHeld(bool);
};


#endif //TETRIS_HOLD_H
