//
// Created by Gek on 01/11/2023.
//

#include <cstring>
#include <algorithm>
#include "PlayingField.h"
#include "Queue.h"
#include "../Game.h"

PlayingField::PlayingField(Game *game)
{
    this->game = game;
    std::fill(&playingField[0][0], &playingField[0][0] + sizeof(playingField)/sizeof(playingField[0][0]), EMPTY);
}

PlayingField::~PlayingField()
{
    delete currentPiece;
}

/**
 * Check if position on playing field is empty.
 *
 * @param pos Coordinates of targeted position.
 * @return true if position is of Type EMPTY, otherwise false.
 */
bool PlayingField::isFree(std::array<int, 2> pos)
{
    int x = pos[0];
    int y = pos[1];
    if (x < 0 || x >= PLAYING_FIELD_WIDTH || y < 0 || y >= PLAYING_FIELD_HEIGHT)
    {
        return false;
    }
    return playingField[x][y] == EMPTY;
}

/**
 * Place piece on playing field according to it's current position.
 * Delete the piece.
 *
 * @param piece Piece being placed.
 */
void PlayingField::placePiece(Piece *piece)
{
    for (auto &blockPosition : piece->getCurrentPosition())
    {
        playingField[blockPosition[0]][blockPosition[1]] = piece->getPieceType();
    }
    clearLinesIfPossible();
    delete piece;

    game->getQueue()->shiftQueue();
    game->getHold()->setAlreadyHeld(false);
}

/**
 * Set a row of cells to empty and shift all cells above one position lower.
 *
 * @param lineIndex Y coordinate of line to be cleared.
 */
void PlayingField::clearLine(int lineIndex)
{
    for (auto & fieldCell : playingField)
    {
        for (int j = lineIndex; j < PLAYING_FIELD_HEIGHT-1; j++)
        {
            fieldCell[j] = fieldCell[j+1];
        }
        fieldCell[PLAYING_FIELD_HEIGHT-1] = EMPTY;
    }
}

/**
 * Detect if any line is ready to be cleared.
 * If yes, clear all full lines.
 */
void PlayingField::clearLinesIfPossible()
{
    int linesCleared = 0;
    std::vector<int> clearedLineIndexes = {};
    bool isFull;
    for (int i = PLAYING_FIELD_HEIGHT-1; i >= 0; i--)
    {
        isFull = true;
        for (int j = 0; j < PLAYING_FIELD_WIDTH; j++)
        {
            isFull = playingField[j][i] != EMPTY && isFull;
        }
        if (isFull)
        {
            clearedLineIndexes.push_back(i);
            linesCleared++;
        }
    }

    if (!clearedLineIndexes.empty())
    {
        // clear line effect + delay
        for (int clearedLine : clearedLineIndexes)
        {
            for (int i = 0; i < PLAYING_FIELD_WIDTH; i++)
            {
                playingField[i][clearedLine] = WHITE;
            }
        }
        game->getGraphics()->drawPlayingField();
        Timer::pause(500);
        for (int clearedLine : clearedLineIndexes)
        {
            clearLine(clearedLine);
        }
    }

    game->getScore()->calculateScore(linesCleared, currentPiece->isDidTSpin(), currentPiece->isDidMiniTSpin());
    game->getLevel()->addClearedLines(linesCleared);
}

std::array<int, 2> PlayingField::getPieceSpawnOffset() const
{
    return pieceSpawnOffset;
}

std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH> PlayingField::getPlayingField() const
{
    return playingField;
}

Piece *PlayingField::getCurrentPiece() const
{
    return currentPiece;
}

void PlayingField::setCurrentPiece(Type type)
{
    currentPiece = new Piece(type, game);
    for (std::array<int, 2> block : currentPiece->getCurrentPosition())
    {
        if (game->getPlayingField()->getPlayingField()[block[0]][block[1]] != EMPTY)
        {
            game->gameOver();
            return;
        }
    }
}
