//
// Created by gek on 5.11.23.
//

#ifndef TETRIS_QUEUE_H
#define TETRIS_QUEUE_H

#include <deque>
#include <vector>
#include "piece-tables/PieceType.h"
#include "Piece.h"

class Game;

/**
 * The preview of one or more pieces that follow the current piece.
 */
class Queue
{
private:
    std::deque<Type> queue;
    int visiblePieceCount;
    void generateBag();
    Game* game;
public:
    static const int defaultVisiblePieceCount = 5;
    explicit Queue(int, Game*);
    std::vector<Type> showQueue();
    void shiftQueue();

    [[nodiscard]] int getVisiblePieceCount() const;
};


#endif //TETRIS_QUEUE_H
