//
// Created by Gek on 01/11/2023.
//

#ifndef TETRIS_PLAYINGFIELD_H
#define TETRIS_PLAYINGFIELD_H

#define PLAYING_FIELD_HEIGHT 30
#define PLAYING_FIELD_WIDTH 10

#include <array>
#include "piece-tables/PieceType.h"

class Game;
class Piece;
class Queue;

/**
 * The playing field where in game blocks are placed.
 */
class PlayingField
{
private:
    std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH> playingField = {};
    std::array<int, 2> pieceSpawnOffset = {PLAYING_FIELD_WIDTH/2 - 2, 20};
    Game *game;
    Piece *currentPiece = nullptr;
    void clearLine(int);
public:
    explicit PlayingField(Game*);
    ~PlayingField();
    void placePiece(Piece*);
    bool isFree(std::array<int, 2> pos);
    void clearLinesIfPossible();

    [[nodiscard]] std::array<int, 2> getPieceSpawnOffset() const;
    [[nodiscard]] std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH> getPlayingField() const;
    [[nodiscard]] Piece *getCurrentPiece() const;
    void setCurrentPiece(Type);
};


#endif //TETRIS_PLAYINGFIELD_H
