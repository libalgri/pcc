//
// Created by Gek on 01/11/2023.
//

#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include "Piece.h"
#include "PlayingField.h"
#include "../Game.h"

/**
 * Assign piece type and set Piece's initial position and rotation center.
 *
 * @param pieceType The Type of the Piece.
 * @param game The Game the Piece belongs to.
 */
Piece::Piece(Type pieceType, Game *game)
{
    this->pieceType = pieceType;
    this->game = game;
    setInitialPosition();
}

/**
 * Set currentPosition to default piece position on the playing field.
 * Set currentRotationCenter to default position on the playing field.
 */
void Piece::setInitialPosition()
{
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            currentPosition[i][j] = PieceType::getPieceShape(pieceType).shape[i][j] +
                                    game->getPlayingField()->getPieceSpawnOffset()[j];
        }
    }

    for (int i = 0; i < 2; i++)
    {
        currentRotationCenter[i] = static_cast<float>(game->getPlayingField()->getPieceSpawnOffset()[i]) + PieceType::getRotationCenter(pieceType).position[i];
    }
}

/**
 * Change piece and rotation center offset by number of spaces.
 *
 * @param x Horizontal offset (positive values move the piece towards the left).
 * @param y Vertical offset (positive values move the piece upwards).
 */
bool Piece::move(int x, int y)
{
    std::array<std::array<int, 2>, 4> newPosition = {};
    std::copy(&currentPosition[0][0], &currentPosition[0][0]+4*2, &newPosition[0][0]);

    for (auto & pos : newPosition)
    {
        pos[0] += x;
        pos[1] += y;
        if (!game->getPlayingField()->isFree(pos))
        {
            return false;
        }
    }

    std::copy(&newPosition[0][0], &newPosition[0][0]+4*2, &currentPosition[0][0]);
    currentRotationCenter[0] += static_cast<float>(x);
    currentRotationCenter[1] += static_cast<float>(y);

    didTSpin = false;

    return true;
}

/**
 * Move Piece by an offset repeatedly until no longer possible.
 *
 * @param x Horizontal offset (positive values move the piece towards the left).
 * @param y Vertical offset (positive values move the piece upwards).
 */
void Piece::moveRepeated(int x, int y)
{
    while (move(x, y)) {}
}

/**
 * Rotate piece in given direction according to SRS if possible.
 * If the rotated piece is of Type T, also determine if the rotation results in a T spin or T spin mini.
 *
 * @param rotationType Direction of rotation.
 */
bool Piece::rotate(RotationType rotationType)
{
    std::array<std::array<float, 2>, 4> relativePiecePosition = {};
    std::array<float, 2> oldRotationCenter = currentRotationCenter;

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            relativePiecePosition[i][j] = static_cast<float>(currentPosition[i][j]) - currentRotationCenter[j];
        }
    }

    const float sinValue = rotationType ? -1 : 1;
    std::array<std::array<int, 2>, 4> rotatedPiecePosition = {};

    for (int i = 0; i < 4; i++) // rotation matrix multiplication
    {
        // excluding multiplication with cos(x), as it is always equal to 0.
        rotatedPiecePosition[i][0] = static_cast<int>((relativePiecePosition[i][1] * sinValue) + currentRotationCenter[0]);
        rotatedPiecePosition[i][1] = static_cast<int>((-relativePiecePosition[i][0] * sinValue) + currentRotationCenter[1]);
    }

    kickTable kickTable = PieceType::getKickTable(pieceType, rotationState, rotationType);

    if (!kick(rotatedPiecePosition, kickTable))
    {
        return false;
    }

    int rotationStateChange = rotationType ? -1 : 1;
    int newRotationStateIndex = (rotationState + rotationStateChange) % (LEFT + 1);
    if (newRotationStateIndex == -1)
    {
        rotationState = LEFT;
    }
    else
    {
        rotationState = static_cast<RotationState>(newRotationStateIndex);
    }

    // T spin detection
    if (pieceType == T)
    {
        int corners[4][2] = {{-1, 1},{1, 1},{1, -1},{-1, -1}};
        int filledCornerCount = 0;
        std::array<int, 2> currentCorner = {};
        for (auto & corner : corners)
        {
            currentCorner[0] = static_cast<int>(currentRotationCenter[0]) + corner[0];
            currentCorner[1] = static_cast<int>(currentRotationCenter[1]) + corner[1];
            filledCornerCount += !game->getPlayingField()->isFree(currentCorner);
        }
        if (filledCornerCount >= 3)
        {
            didTSpin = true;
            // T spin mini detection
            didMiniTSpin = false;
            std::array<std::array<int, 2>, 2> frontCorners = {};
            for (int i = 0; i < 2; i++)
            {
                frontCorners[i][0] = static_cast<int>(currentRotationCenter[0]) + corners[(i + rotationState)%4][0];
                frontCorners[i][1] = static_cast<int>(currentRotationCenter[1]) + corners[(i + rotationState)%4][1];
                if (game->getPlayingField()->isFree(frontCorners[i]))
                {
                    didMiniTSpin = true;
                }
            }
            if (oldRotationCenter[1] - currentRotationCenter[1] == 2)
            {
                didMiniTSpin = false;
            }
        }
    }

    return true;
}

/**
 * Attempt to rotate piece using SRS kicks.
 *
 * @param rotatedPiecePosition The coordinates of all the piece's blocks.
 * @param kickTable Kick values by SRS standards.
 * @return true if a viable kick was found, otherwise false.
 */
bool Piece::kick(std::array<std::array<int, 2>, 4> rotatedPiecePosition, kickTable kickTable)
{
    std::array<std::array<int, 2>, 4> kickedPiecePosition = {};
    bool isSuccessful;
    for (int *kickValue : kickTable.kicks)
    {
        isSuccessful = true;
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                kickedPiecePosition[i][j] = rotatedPiecePosition[i][j] + kickValue[j];
            }
            isSuccessful = game->getPlayingField()->isFree(kickedPiecePosition[i]) && isSuccessful;
        }
        if (isSuccessful)
        {
            std::copy(&kickedPiecePosition[0][0], &kickedPiecePosition[0][0]+4*2, &currentPosition[0][0]);
            for (int i = 0; i < 2; i++)
            {
                currentRotationCenter[i] += static_cast<float>(kickValue[i]);
            }
            return true;
        }
    }

    return false;
}

/**
 * Place piece on playing field.
 */
void Piece::placeSelf()
{
    game->getPlayingField()->placePiece(this);
}

/**
 * Put piece in hold slot.
 */
void Piece::holdSelf()
{
    game->getHold()->hold(this);
}

Type Piece::getPieceType() const
{
    return pieceType;
}

const std::array<std::array<int, 2>, 4> &Piece::getCurrentPosition() const
{
    return currentPosition;
}

bool Piece::isDidTSpin() const
{
    return didTSpin;
}

bool Piece::isDidMiniTSpin() const
{
    return didMiniTSpin;
}
