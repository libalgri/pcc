//
// Created by Gek on 01/11/2023.
//

#ifndef TETRIS_PIECE_H
#define TETRIS_PIECE_H

#include <array>
#include "piece-tables/PieceType.h"
#include "piece-tables/RotationType.h"

class Game;

/**
 * The tetromino that the player controls.
 */
class Piece {
private:
    RotationState rotationState = DEFAULT;
    bool didTSpin = false;
    bool didMiniTSpin = false;
    Type pieceType;
    std::array<float, 2> currentRotationCenter = {};
    std::array<std::array<int, 2>, 4> currentPosition = {};
    Game *game;
    void setInitialPosition();
    bool kick(std::array<std::array<int, 2>, 4>, kickTable);
public:
    Piece(Type, Game*);
    bool move(int, int);
    void moveRepeated(int, int);
    bool rotate(RotationType);
    void placeSelf();
    void holdSelf();

    // Getters and setters
    [[nodiscard]] Type getPieceType() const;
    [[nodiscard]] const std::array<std::array<int, 2>, 4> &getCurrentPosition() const;
    [[nodiscard]] bool isDidTSpin() const;
    [[nodiscard]] bool isDidMiniTSpin() const;
};


#endif //TETRIS_PIECE_H
