//
// Created by Gek on 02/11/2023.
//

#ifndef TETRIS_ROTATIONTYPE_H
#define TETRIS_ROTATIONTYPE_H

/**
 * Types of rotations a player can do.
 */
enum RotationType {CLOCKWISE, COUNTER_CLOCKWISE};

/**
 * The rotation a piece can have relative to it's default position.
 */
enum RotationState
{
    DEFAULT,
    RIGHT,
    DOUBLE,
    LEFT
};

#endif //TETRIS_ROTATIONTYPE_H
