//
// Created by Gek on 02/11/2023.
//

#ifndef TETRIS_PIECETYPE_H
#define TETRIS_PIECETYPE_H

#include <string>
#include "RotationType.h"

/**
 * The seven different tetromino types with extra values used in graphics.
 */
enum Type {T, L, J, S, Z, I, O, EMPTY, GHOST, WHITE};

/**
 * A structure representing the coordinates of each of the 4 blocks of a tetromino.
 */
struct pieceShape {
    int shape[4][2];
};

/**
 * A set of positions relative to the piece's position that are checked for availability when performing a rotation.
 */
struct kickTable {
    int kicks[5][2];
};

/**
 * The relative coordinates of a piece's center of rotation.
 */
struct rotationCenter {
    float position[2];
};

/**
 * Definitions of static tables helping with piece shape and rotation.
 */
class PieceType {
private:
    inline static pieceShape pieceShapeTable[7] = {
            (pieceShape){.shape = {{0,0},{1,0},{1,1},{2,0}}},
            (pieceShape){.shape = {{0,0},{1,0},{2,0},{2,1}}},
            (pieceShape){.shape = {{0,1},{0,0},{1,0},{2,0}}},
            (pieceShape){.shape = {{0,0},{1,0},{1,1},{2,1}}},
            (pieceShape){.shape = {{0,1},{1,1},{1,0},{2,0}}},
            (pieceShape){.shape = {{0,0},{1,0},{2,0},{3,0}}},
            (pieceShape){.shape = {{1,1},{2,1},{1,0},{2,0}}}
    };

    inline static kickTable regularKickTable[4] = {
            (kickTable){.kicks{{0,0},{-1,0},{-1,1},{0,-2},{-1,-2}}},
            (kickTable){.kicks{{0, 0}, {1, 0},{1, -1},{0, 2},{1, 2}}},
            (kickTable){.kicks{{0, 0},{1, 0},{1, 1},{0, -2},{1, -2}}},
            (kickTable){.kicks{{0, 0},{-1, 0},{-1, -1},{0, 2},{-1, 2}}}
    };

    inline static kickTable iPieceKickTable[4] = {
            (kickTable){.kicks{{0, 0},{-2, 0},{1, 0},{-2, -1},{1, 2}}},
            (kickTable){.kicks{{0, 0},{-1, 0},{2, 0},{-1, 2},{2, -1}}},
            (kickTable){.kicks{{0, 0},{2, 0},{-1, 0},{2, 1},{-1, -2}}},
            (kickTable){.kicks{{0, 0},{1, 0},{-2, 0},{1, -2},{-2, 1}}}
    };

    inline static rotationCenter rotationCenterTable[7] = {
            (rotationCenter){.position{1, 0}},
            (rotationCenter){.position{1, 0}},
            (rotationCenter){.position{1, 0}},
            (rotationCenter){.position{1, 0}},
            (rotationCenter){.position{1, 0}},
            (rotationCenter){.position{1.5, -0.5}},
            (rotationCenter){.position{1.5, 0.5}}
    };

    inline static std::string pieceColorTable[10] = {
            "\x1B[48:5:91m",
            "\x1B[48:5:202m",
            "\x1B[48:5:19m",
            "\x1B[48:5:10m",
            "\x1B[48:5:9m",
            "\x1B[48:5:14m",
            "\x1B[48:5:11m",
            "\x1B[48:5:0m",
            "\x1B[48:5:238m",
            "\x1B[48:5:15m"
    };

public:
    static pieceShape getPieceShape(Type);
    static rotationCenter getRotationCenter(Type);
    static kickTable getKickTable(Type, RotationState, RotationType);
    static std::string getPieceColor(Type);
};


#endif //TETRIS_PIECETYPE_H
