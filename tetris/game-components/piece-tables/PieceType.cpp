//
// Created by Gek on 02/11/2023.
//

#include <vector>
#include <algorithm>
#include <iterator>
#include <cstring>
#include "PieceType.h"
#include "RotationType.h"

/**
 * Get default block coordinates for specified piece.
 *
 * @param pieceType Type of the piece.
 * @return The corresponding pieceShape.
 */
pieceShape PieceType::getPieceShape(Type pieceType)
{
    pieceShape shape = pieceShapeTable[pieceType];
    return shape;
}

/**
 * Return set of wall kicks for specified piece and rotation.
 *
 * @param pieceType Type of piece being rotated.
 * @param rotationState The RotationState the piece is in before the rotation.
 * @param rotationType The direction of the rotation.
 * @return The corresponding kickTable.
 */
kickTable PieceType::getKickTable(Type pieceType, RotationState rotationState, RotationType rotationType)
{
    kickTable result = {};
    kickTable *kickTableSelection;
    if (pieceType == I)
    {
        kickTableSelection = iPieceKickTable;
    }
    else
    {
        kickTableSelection = regularKickTable;
    }

    int kickIndex = rotationState;

    if (rotationType == COUNTER_CLOCKWISE) {
        if (kickIndex == 0) {
            kickIndex = 3;
        } else {
            kickIndex--;
        }
        std::memcpy(result.kicks, kickTableSelection[kickIndex].kicks, sizeof(kickTableSelection[kickIndex].kicks));
        int i;
        for (i = 0; i < sizeof(result.kicks) / sizeof(result.kicks[0]); i++)
        {
            result.kicks[i][0] *= -1;
            result.kicks[i][1] *= -1;
        }
    }
    else
    {
        std::memcpy(result.kicks, kickTableSelection[kickIndex].kicks, sizeof(kickTableSelection[kickIndex].kicks));
    }

    return result;
}

/**
 * Get the relative coordinates of the piece's rotation center.
 * @param pieceType The Type of the Piece.
 * @return The corresponding rotationCenter.
 */
rotationCenter PieceType::getRotationCenter(Type pieceType)
{
    rotationCenter rotationCenter = PieceType::rotationCenterTable[pieceType];
    return rotationCenter;
}

/**
 * Get the ANSI escape sequence representing a piece type's color in the terminal.
 * @param type The type of the Piece.
 * @return ANSI escape sequence as a string.
 */
std::string PieceType::getPieceColor(Type type)
{
    return pieceColorTable[type];
}
