//
// Created by gek on 5.11.23.
//

#include <algorithm>
#include <numeric>
#include <random>
#include <array>
#include <chrono>
#include "Queue.h"
#include "../Game.h"

/**
 * Create queue and generate enough bags to initially fill queue.
 *
 * @param visiblePieceCount Number of pieces in Queue that are visible for the player.
 */
Queue::Queue(int visiblePieceCount, Game *game)
{
    this->visiblePieceCount = visiblePieceCount;
    this->game = game;
    for (int i = 0; i <= (visiblePieceCount-1)/7; i++)
    {
        generateBag();
    }
}

/**
 * Generate randomly shuffled sequence of 7 piece types (also known as a "bag").
 */
void Queue::generateBag()
{
    std::array<int, 7> newBag = {};
    std::iota(newBag.begin(), newBag.end(), 0);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(newBag.begin(), newBag.end(), std::default_random_engine(seed));

    for (int pieceType : newBag)
    {
        queue.push_back(static_cast<Type>(pieceType));
    }
}

/**
 * Return visible part of queue.
 *
 * @return array of piece types.
 */
std::vector<Type> Queue::showQueue()
{
    std::vector<Type> visibleQueue = {};
    for (int i = 0; i < visiblePieceCount; i++)
    {
        visibleQueue.push_back(queue[(visiblePieceCount-1)-i]);
    }
    return visibleQueue;
}

/**
 * Return to-be-current piece from queue.
 * Check if piece can be placed on the playing field.
 * Generate an extra bag if needed.
 */
void Queue::shiftQueue()
{
    game->getPlayingField()->setCurrentPiece(queue.front());

    queue.pop_front();

    if (queue.size() < visiblePieceCount)
    {
        generateBag();
    }
}

int Queue::getVisiblePieceCount() const
{
    return visiblePieceCount;
}
