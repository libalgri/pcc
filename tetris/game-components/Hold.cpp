//
// Created by gek on 13.11.23.
//

#include "Hold.h"
#include "Piece.h"
#include "../Game.h"

Hold::Hold(Game *game)
{
    this->game = game;
}

/**
 * Switch the current piece for the piece in the hold slot.
 * If the hold slot is empty, place the current piece into the slot and take the next piece from the Queue.
 *
 * @param piece The piece to be held.
 */
void Hold::hold(Piece *piece)
{
    if (alreadyHeld)
    {
        return;
    }
    if (heldPieceType == EMPTY)
    {
        heldPieceType = piece->getPieceType();
        game->getQueue()->shiftQueue();
        delete piece;
        alreadyHeld = true;
        return;
    }

    Type oldPieceType = piece->getPieceType();
    game->getPlayingField()->setCurrentPiece(heldPieceType);
    heldPieceType = oldPieceType;
    alreadyHeld = true;

    delete piece;
}

Type Hold::getHeldPieceType()
{
    return heldPieceType;
}

void Hold::setAlreadyHeld(bool held)
{
    alreadyHeld = held;
}
