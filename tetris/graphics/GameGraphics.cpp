//
// Created by gek on 13.11.23.
//

#include <iostream>
#include <vector>
#include "GameGraphics.h"
#include "../game-components/PlayingField.h"
#include "../Game.h"

GameGraphics::GameGraphics(Game *game)
{
    this->game = game;
}

/**
 * Draw full playing field with Hold slot and Queue included.
 */
void GameGraphics::drawPlayingField()
{
    std::string result = "\x1B[2J\x1B[H";

    std::array<std::string, HOLD_SLOT_HEIGHT> holdSlot = generateHoldSlot();
    std::array<std::string, QUEUE_HEIGHT> queue = generateQueue();
    std::array<std::string, SCORE_COUNTER_HEIGHT> scoreCounter = generateScoreCounter();

    std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH> playingField(game->getPlayingField()->getPlayingField());
    std::array<std::array<int, 2>, 4> currentPiecePosition(game->getPlayingField()->getCurrentPiece()->getCurrentPosition());

    addCurrentPiece(playingField);
    addGhostPiece(playingField);

    result += horizontalLine(3) + "HOLD" +
            horizontalLine(PLAYING_FIELD_WIDTH*2 + 6) + "QUEUE" +
            horizontalLine(2) + "\n";

    for (int i = VISIBLE_PLAYING_FIELD_HEIGHT-1; i >= 0; i--)
    {
        if (i >= VISIBLE_PLAYING_FIELD_HEIGHT-HOLD_SLOT_HEIGHT)
        {
            result += holdSlot[i - (VISIBLE_PLAYING_FIELD_HEIGHT-HOLD_SLOT_HEIGHT)];
        }
        else
        {
            result += std::string(HOLD_SLOT_WIDTH, ' ');
        }

        result += '|';
        for (int j = 0; j < PLAYING_FIELD_WIDTH; j++)
        {
            result += generateBlock(static_cast<Type>(playingField[j][i]));
        }
        result += '|';

        if (i >= VISIBLE_PLAYING_FIELD_HEIGHT-QUEUE_HEIGHT)
        {
            result += queue[i - (VISIBLE_PLAYING_FIELD_HEIGHT-QUEUE_HEIGHT)];
        }

        if (i >= VISIBLE_PLAYING_FIELD_HEIGHT-QUEUE_HEIGHT-SCORE_COUNTER_HEIGHT && i < VISIBLE_PLAYING_FIELD_HEIGHT-QUEUE_HEIGHT)
        {
            result += scoreCounter[i - (VISIBLE_PLAYING_FIELD_HEIGHT-QUEUE_HEIGHT-SCORE_COUNTER_HEIGHT)];
        }

        result += "\n";
    }

    result += std::string(HOLD_SLOT_WIDTH, ' ') + horizontalLine(PLAYING_FIELD_WIDTH*2 + 2) + "\n";

    std::cout << result;
}

/**
 * Generate string representing hold slot.
 *
 * @return Hold slot as an array of strings.
 */
std::array<std::string, 3> GameGraphics::generateHoldSlot()
{
    std::array<std::string, 3> result = {};
    std::array<std::array<Type, HOLD_SLOT_HEIGHT-1>, 4> cellTypes = {};
    std::fill(&cellTypes[0][0], &cellTypes[0][0] + sizeof(cellTypes)/sizeof(cellTypes[0][0]), EMPTY);
    Type holdPieceType = game->getHold()->getHeldPieceType();

    result[0] = horizontalLine(HOLD_SLOT_WIDTH);

    if (holdPieceType != EMPTY)
    {
        for (int *blockPosition : PieceType::getPieceShape(holdPieceType).shape)
        {
            cellTypes[blockPosition[0]][blockPosition[1]] = holdPieceType;
        }
    }

    for (int i = 1; i < HOLD_SLOT_HEIGHT; i++)
    {
        result[i] += '|';
        for (int j = 0; j < 4; j++)
        {
            result[i] += generateBlock(cellTypes[j][i-1]);
        }
    }

    return result;
}

/**
 * Generate string representing current queue.
 *
 * @return Queue as an array of strings.
 */
std::array<std::string, QUEUE_HEIGHT> GameGraphics::generateQueue()
{
    std::array<std::string, QUEUE_HEIGHT> result = {};
    std::array<std::array<Type, QUEUE_HEIGHT-1>, 4> cellTypes = {};
    std::fill(&cellTypes[0][0], &cellTypes[0][0] + sizeof(cellTypes)/sizeof(cellTypes[0][0]), EMPTY);
    std::vector<Type> queuePieceTypes = game->getQueue()->showQueue();

    result[0] = horizontalLine(QUEUE_WIDTH);

    for (int i = 0; i < game->getQueue()->getVisiblePieceCount(); i++)
    {
        for (int *blockPosition : PieceType::getPieceShape(queuePieceTypes[i]).shape)
        {
            cellTypes[blockPosition[0]][blockPosition[1] + i*3] = queuePieceTypes[i];
        }
    }

    for (int i = 1; i < QUEUE_HEIGHT; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            result[i] += generateBlock(cellTypes[j][i-1]);
        }
        result[i] += '|';
    }

    return result;
}

/**
 * Generate string representing a score and level tracker.
 *
 * @return Score and level tracker as an array of strings.
 */
std::array<std::string, SCORE_COUNTER_HEIGHT> GameGraphics::generateScoreCounter()
{
    std::array<std::string, SCORE_COUNTER_HEIGHT> result = {};
    result[4] = " LEVEL:";
    result[3] = " " + std::to_string(game->getLevel()->getCurrentLevel());
    result[1] = " SCORE:";
    result[0] = " " + std::to_string(game->getScore()->getScore());

    return result;
}

/**
 * Add ghost piece (shadow piece) to playing field.
 *
 * @param playingField 2D array representing the current playing field.
 */
void GameGraphics::addGhostPiece(std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH> &playingField)
{
    Piece ghost(*game->getPlayingField()->getCurrentPiece());
    ghost.moveRepeated(0, -1);
    for (std::array<int, 2> blockPosition : ghost.getCurrentPosition())
    {
        if (playingField[blockPosition[0]][blockPosition[1]] == EMPTY)
        {
            playingField[blockPosition[0]][blockPosition[1]] = GHOST;
        }
    }
}

/**
 * Add the current piece to visual representation of the playing field.
 *
 * @param playingField 2D array representing the current playing field.
 */
void GameGraphics::addCurrentPiece(std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH> &playingField)
{
    Type currentPieceType = game->getPlayingField()->getCurrentPiece()->getPieceType();
    for (std::array<int, 2> blockPosition : game->getPlayingField()->getCurrentPiece()->getCurrentPosition())
    {
        if (playingField[blockPosition[0]][blockPosition[1]] == EMPTY)
        {
            playingField[blockPosition[0]][blockPosition[1]] = currentPieceType;
        }
    }
}

/**
 * Generate a single colored block.
 * If allPiecesGray is true, always generate a gray block.
 *
 * @param type Type of the block to be generated.
 * @return String representing the colored block.
 */
std::string GameGraphics::generateBlock(Type type) const
{
    Type colorType = allPiecesGray && type != EMPTY ? GHOST : type;
    std::string result;
    result += PieceType::getPieceColor(colorType) + "  " + PieceType::getPieceColor(EMPTY);
    return result;
}

/**
 * Generate string for horizontal line of set length.
 *
 * @param length Length of horizontal line in character count.
 * @return String representing the line.
 */
std::string GameGraphics::horizontalLine(int length)
{
    return std::string(length, '-');
}

/**
 * Set the color of all pieces to gray.
 *
 * @param isGray
 */
void GameGraphics::setAllPiecesGray(bool isGray)
{
    allPiecesGray = isGray;
}


