//
// Created by gek on 13.11.23.
//

#ifndef TETRIS_GAMEGRAPHICS_H
#define TETRIS_GAMEGRAPHICS_H

#include <array>
#include "../game-components/piece-tables/PieceType.h"
#include "../game-components/PlayingField.h"

#define HOLD_SLOT_WIDTH 9
#define HOLD_SLOT_HEIGHT 3
#define QUEUE_WIDTH 9
#define QUEUE_HEIGHT 15
#define VISIBLE_PLAYING_FIELD_HEIGHT 22
#define SCORE_COUNTER_HEIGHT 5

class Game;

/**
 * Class used to visually represent the playing field, queue, hold piece and score in the terminal.
 */
class GameGraphics
{
private:
    Game *game;
    bool allPiecesGray = false;
    static std::string horizontalLine(int);
    std::array<std::string, HOLD_SLOT_HEIGHT> generateHoldSlot();
    std::array<std::string, QUEUE_HEIGHT> generateQueue();
    std::array<std::string, SCORE_COUNTER_HEIGHT> generateScoreCounter();
    [[nodiscard]] std::string generateBlock(Type) const;
    void addGhostPiece(std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH>&);
    void addCurrentPiece(std::array<std::array<Type, PLAYING_FIELD_HEIGHT>, PLAYING_FIELD_WIDTH>&);

public:
    explicit GameGraphics(Game*);
    void drawPlayingField();
    void setAllPiecesGray(bool);
};


#endif //TETRIS_GAMEGRAPHICS_H
